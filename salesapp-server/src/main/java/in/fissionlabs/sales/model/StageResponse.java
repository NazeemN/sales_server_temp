package in.fissionlabs.sales.model;

public class StageResponse extends ClientResponse {

	private Stage stage;

	public StageResponse(int responseCode, String responseMessage, Stage stage) {
		super(responseCode, responseMessage);
		this.stage = stage;
	}

	public Stage getStage() {
		return stage;
	}

	public void setStage(Stage stage) {
		this.stage = stage;
	}

}
