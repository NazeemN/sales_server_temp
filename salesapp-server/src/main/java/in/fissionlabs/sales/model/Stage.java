package in.fissionlabs.sales.model;

import java.io.Serializable;

public class Stage implements Serializable {

	private static final long serialVersionUID = -5948858023807688061L;

	private int id;
	private String stage;
	private int companyId;

	public Stage(int id, String stage, int companyId) {
		super();
		this.id = id;
		this.stage = stage;
		this.companyId = companyId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getStage() {
		return stage;
	}

	public void setStage(String stage) {
		this.stage = stage;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

}
