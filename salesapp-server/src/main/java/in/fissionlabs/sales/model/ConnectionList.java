package in.fissionlabs.sales.model;

import java.util.List;

public class ConnectionList extends ClientResponse {
	private List<Conection> connections;

	public ConnectionList(int responseCode, String responseMessage,
			List<Conection> connections) {
		super(responseCode, responseMessage);
		this.connections = connections;
	}

	public List<Conection> getConnections() {
		return connections;
	}

	public void setConnections(List<Conection> connections) {
		this.connections = connections;
	}

}
