package in.fissionlabs.sales.model;

import java.io.Serializable;

public class Conection implements Serializable {

	private static final long serialVersionUID = -4924800448694305830L;
	private int connectionId;
	private String connectedFrom;
	private String connectedTo;
	private String connectedThrough;
	private String level;
	private int companyId;

	public Conection(int connectionId, String connectedFrom,
			String connectedTo, String connectedThrough, String level,
			int companyId) {
		super();
		this.connectionId = connectionId;
		this.connectedFrom = connectedFrom;
		this.connectedTo = connectedTo;
		this.connectedThrough = connectedThrough;
		this.level = level;
		this.companyId = companyId;
	}

	public int getConnectionId() {
		return connectionId;
	}

	public void setConnectionId(int connectionId) {
		this.connectionId = connectionId;
	}

	public String getConnectedFrom() {
		return connectedFrom;
	}

	public void setConnectedFrom(String connectedFrom) {
		this.connectedFrom = connectedFrom;
	}

	public String getConnectedTo() {
		return connectedTo;
	}

	public void setConnectedTo(String connectedTo) {
		this.connectedTo = connectedTo;
	}

	public String getConnectedThrough() {
		return connectedThrough;
	}

	public void setConnectedThrough(String connectedThrough) {
		this.connectedThrough = connectedThrough;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

}
