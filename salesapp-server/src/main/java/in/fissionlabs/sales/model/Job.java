package in.fissionlabs.sales.model;

import java.io.Serializable;

public class Job implements Serializable {

	private static final long serialVersionUID = 1721090515458663437L;
	private int jobId;
	private String title;
	private String description;
	private int companyId;

	public Job(int jobId, String title, String description, int companyId) {
		super();
		this.jobId = jobId;
		this.title = title;
		this.description = description;
		this.companyId = companyId;
	}

	public int getJobId() {
		return jobId;
	}

	public void setJobId(int jobId) {
		this.jobId = jobId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

}
