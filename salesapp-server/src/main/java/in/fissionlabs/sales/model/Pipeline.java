package in.fissionlabs.sales.model;

public class Pipeline {

	private int id;
	private String status;
	private String stage;
	private int companyId;

	public Pipeline(int id, String status, String stage, int companyId) {
		super();
		this.id = id;
		this.status = status;
		this.stage = stage;
		this.companyId = companyId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStage() {
		return stage;
	}

	public void setStage(String stage) {
		this.stage = stage;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

}
