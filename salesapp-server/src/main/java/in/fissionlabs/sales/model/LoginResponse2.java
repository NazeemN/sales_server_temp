package in.fissionlabs.sales.model;

import java.util.List;

public class LoginResponse2 extends ClientResponse {
	private List<String> privileges;
	private List<Home> companyList;

	public List<String> getPrivileges() {
		return privileges;
	}

	public void setPrivileges(List<String> privileges) {
		this.privileges = privileges;
	}

	public List<Home> getCompanyList() {
		return companyList;
	}

	public void setCompanyList(List<Home> companyList) {
		this.companyList = companyList;
	}

	public LoginResponse2() {
		super();
	}

	public LoginResponse2(int responseCode, String responseMessage,
			List<String> privileges, List<Home> companyList) {
		super(responseCode, responseMessage);
		this.companyList = companyList;
		this.privileges = privileges;
	}

}
