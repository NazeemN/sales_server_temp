package in.fissionlabs.sales.model;

public class UserNPrivilegeResponse extends ClientResponse {

	private UserNPrivilege user;

	public UserNPrivilegeResponse(int responseCode, String responseMessage,
			UserNPrivilege user) {
		super(responseCode, responseMessage);
		this.user = user;
	}

	public UserNPrivilege getUser() {
		return user;
	}

	public void setUser(UserNPrivilege user) {
		this.user = user;
	}

}
