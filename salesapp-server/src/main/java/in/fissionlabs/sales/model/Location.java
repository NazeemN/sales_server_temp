package in.fissionlabs.sales.model;

import java.io.Serializable;

public class Location implements Serializable {

	private static final long serialVersionUID = 1938248742773760204L;
	private int locationId;
	private String isHeadQuarters;
	/*
	 * private String address_line1; private String address_line2; private
	 * String street;
	 */
	private String address;
	private String city;
	private String state;
	private String country;
	private String zip;
	private int companyId;

	public Location(int locationId, String isHeadQuarters, String address,
	/* String address_line1, String address_line2, String street, */
	String city, String state, String country, String zip, int companyId) {
		super();
		this.locationId = locationId;
		this.isHeadQuarters = isHeadQuarters;
		/*
		 * this.address_line1 = address_line1; this.address_line2 =
		 * address_line2; this.street = street;
		 */
		this.address = address;
		this.city = city;
		this.state = state;
		this.country = country;
		this.zip = zip;
		this.companyId = companyId;
	}

	public int getLocationId() {
		return locationId;
	}

	public void setLocationId(int locationId) {
		this.locationId = locationId;
	}

	public String getIsHeadQuarters() {
		return isHeadQuarters;
	}

	public void setIsHeadQuarters(String isHeadQuarters) {
		this.isHeadQuarters = isHeadQuarters;
	}

	/*
	 * public String getAddress_line1() { return address_line1; }
	 * 
	 * public void setAddress_line1(String address_line1) { this.address_line1 =
	 * address_line1; }
	 * 
	 * public String getAddress_line2() { return address_line2; }
	 * 
	 * public void setAddress_line2(String address_line2) { this.address_line2 =
	 * address_line2; }
	 * 
	 * public String getStreet() { return street; }
	 * 
	 * public void setStreet(String street) { this.street = street; }
	 */

	public String getCity() {
		return city;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

}
