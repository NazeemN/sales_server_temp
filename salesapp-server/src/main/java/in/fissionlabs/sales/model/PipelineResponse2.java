package in.fissionlabs.sales.model;

public class PipelineResponse2 extends ClientResponse {

	private Pipeline2 pipeline;

	public PipelineResponse2(int responseCode, String responseMessage,
			Pipeline2 pipeline) {
		super(responseCode, responseMessage);
		this.pipeline = pipeline;
	}

	public Pipeline2 getPipeline() {
		return pipeline;
	}

	public void setPipeline(Pipeline2 pipeline) {
		this.pipeline = pipeline;
	}
	
}
