package in.fissionlabs.sales.model;

import java.util.List;

public class LoginResponse3 extends ClientResponse {

	private List<Company> completeDataList;

	public LoginResponse3(int responseCode, String responseMessage,
			List<Company> completeDataList) {
		super(responseCode, responseMessage);
		this.completeDataList = completeDataList;
	}

	public List<Company> getCompleteDataList() {
		return completeDataList;
	}

	public void setCompleteDataList(List<Company> completeDataList) {
		this.completeDataList = completeDataList;
	}

}
