package in.fissionlabs.sales.model;

public class User {

	private int id;
	private String emailId;
	private String fullname;
	private String username;
	private String password;
	private String password2;

	public User(String username, String password) {
		super();
		this.username = username;
		this.password = password;
	}

	public User(int id, String emailId, String fullname, String username,
			String password, String password2) {
		super();
		this.id = id;
		this.emailId = emailId;
		this.fullname = fullname;
		this.username = username;
		this.password = password;
		this.password2 = password2;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPassword2() {
		return password2;
	}

	public void setPassword2(String password2) {
		this.password2 = password2;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUserName() {
		return username;
	}

	public void setusername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
