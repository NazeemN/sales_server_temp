package in.fissionlabs.sales.model;

public class Remark {

	private int id;
	private String date;
	private String comments;
	private int companyId;

	public Remark(int id, String date, String comments, int companyId) {
		super();
		this.id = id;
		this.date = date;
		this.comments = comments;
		this.companyId = companyId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

}
