package in.fissionlabs.sales.model;

public class Pipeline2 {

	private int id;
	private Status status;
	private Stage2 stage;
	private int companyId;

	public Pipeline2(int id, Status status, Stage2 stage, int companyId) {
		super();
		this.id = id;
		this.status = status;
		this.stage = stage;
		this.companyId = companyId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Stage2 getStage() {
		return stage;
	}

	public void setStage(Stage2 stage) {
		this.stage = stage;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}
	
	@Override
	public String toString() {
		return "id - "+id+" stageName - "+this.stage.getName()+" stageId - "+this.stage.getId()
				+" statusId - "+this.status.getId()+" statusName - "+this.status.getName()+
				" statusColor "+this.status.getColour();
	}

}
