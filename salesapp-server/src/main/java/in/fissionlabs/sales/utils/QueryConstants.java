package in.fissionlabs.sales.utils;

public class QueryConstants {

	public static final String GET_PROFILE_ID = "select id from profile where company_id=?";
	
	public static final String CHECK_COMPANY1 = "select * from company where name=?";

	public static final String CHECK_COMPANY = "select * from company where name=? and url=? and added_by=?";
	/*
	 * public static final String CHECK_COMPANY_COMPETITOR =
	 * "select * from competitors where company_id=? and name =? and source=?";
	 * public static final String CHECK_COMPANY_TECHSTACK =
	 * "select * from techstack where company_id=? and technology_name=?";
	 * public static final String CHECK_COMPANY_FUNDING =
	 * "select * from funding where company_id=? and amount=?"; public static
	 * final String CHECK_COMPANY_JOB =
	 * "select * from jobs where company_id=? and title=?"; public static final
	 * String CHECK_COMPANY_LOCATION =
	 * "select * from locations where company_id=? and address_line1=? and address_line2=? and street=? and city=? and state=? and country=? and zip=?"
	 * ; public static final String CHECK_COMPANY_CONNECTION =
	 * "select * from connections where company_id=? and connected_from=? and connected_to=?"
	 * ;
	 */
	public static final String CHECK_COMPANY_COMPETITOR = "select * from competitors where company_id=?";
	public static final String CHECK_COMPANY_TECHSTACK = "select * from techstack where company_id=?";
	public static final String CHECK_COMPANY_FUNDING = "select * from funding where company_id=?";
	public static final String CHECK_COMPANY_JOB = "select * from jobs where company_id=?";
	public static final String CHECK_COMPANY_LOCATION = "select * from locations where company_id=?";
	public static final String CHECK_COMPANY_CONNECTION = "select * from connections where company_id=?";

	public static final String ADD_COMPANY = "insert into company(name,url,added_by) values(?,?,?)";
	public static final String ADD_COMPANY_PROFILE = "insert into profile(description,specialities,company_id) values(?,?,?)";
	public static final String ADD_COMPANY_SUMMARY = "insert into summary(founded_by,founded_year,employees_count,ceo,cfo,vp,profile_id) values(?,?,?,?,?,?,?)";
	public static final String ADD_COMPANY_COMPETITOR = "insert into competitors(name,source,company_id) values(?,?,?)";
	public static final String ADD_COMPANY_TECHSTACK = "insert into techstack(technology_name,company_id) values(?,?)";
	public static final String ADD_COMPANY_FUNDING = "insert into funding(type,date,amount,company_id) values(?,?,?,?)";
	public static final String ADD_COMPANY_JOB = "insert into jobs(title,description,company_id) values(?,?,?)";
	// public static final String ADD_COMPANY_LOCATION =
	// "insert into locations(is_head_quarters,address_line1,address_line2,street,city,state,country,zip,company_id) values(?,?,?,?,?,?,?,?,?)";
	public static final String ADD_COMPANY_LOCATION = "insert into locations(is_head_quarters,address,city,state,country,zip,company_id) values(?,?,?,?,?,?,?)";
	public static final String ADD_COMPANY_CONNECTION = "insert into connections(connected_from,connected_to,connected_through,level,company_id) values(?,?,?,?,?)";

	public static final String GET_COMPANY_HOME = "select * from company where id=?";
	public static final String GET_COMPANY_PROFILE = "select * from profile where company_id=?";
	public static final String GET_COMPANY_SUMMARY = "select * from summary where profile_id=("
			+ GET_PROFILE_ID + ")";
	public static final String GET_COMPANY_COMPETITORS = "select * from competitors where company_id=?";
	public static final String GET_COMPANY_TECHSTACK = "select * from techstack where company_id=?";
	public static final String GET_COMPANY_FUNDINGS = "select * from  funding where company_id=?";
	public static final String GET_COMPANY_JOBS = "select * from jobs where company_id=?";
	public static final String GET_COMPANY_LOCATIONS = "select * from locations where company_id=?";
	public static final String GET_COMPANY_CONNECTIONS = "select * from connections where company_id=?";

	public static final String GET_PASSWORD = "select password from users where username=?";
	public static final String GET_ROLES = "select role from privileges where id in (select privilege_id from users_privileges where username=?)";

	public static final String UPDATE_COMPANY = "update company set name=?,url=? where id=?";
	public static final String UPDATE_COMPANY_PROFILE = "update profile set description=?,specialities=?,company_id=? where id=?";
	public static final String UPDATE_COMPANY_SUMMARY = "update summary set founded_by=?,founded_year=?,employees_count=?,ceo=?,cfo=?,vp=?,profile_id=? where id=?";
	public static final String UPDATE_COMPANY_COMPETITOR = "update competitors set name=?,source=? where id=?";
	public static final String UPDATE_COMPANY_TECHSTACK = "update techstack set technology_name=? where id=?";
	public static final String UPDATE_COMPANY_FUNDING = "update funding set type=?,date=?,amount=? where id=?";
	public static final String UPDATE_COMPANY_JOB = "update jobs set title=?,description=? where id=?";
	// public static final String UPDATE_COMPANY_LOCATION =
	// "update locations set is_head_quarters=?,address_line1=?,address_line2=?,street=?,city=?,state=?,country=?,zip=? where id=?";
	public static final String UPDATE_COMPANY_LOCATION = "update locations set is_head_quarters=?,address=?,city=?,state=?,country=?,zip=? where id=?";
	public static final String UPDATE_COMPANY_CONNECTION = "update connections set connected_from=?,connected_to=?,connected_through=?,level=? where id=?";

	public static final String GET_COMPANYLIST = "select * from company where added_by=?";

	public static final String GET_PRIVILEGE_ID = "select privilege_id from users_privileges where username=?";

	public static final String GET_COMPLETE_COMPANYLIST = "select * from company";

	public static final String LOCATION_COMPANYLIST = "SELECT * FROM company WHERE id IN (SELECT company_id FROM locations WHERE "
			+ "address LIKE ? OR city LIKE ? OR "
			+ "state LIKE ? OR country LIKE ?)";

	public static final String TECHNOLOGY_COMPANYLIST = "SELECT * FROM company WHERE id IN (SELECT company_id FROM techstack WHERE "
			+ "technology_name LIKE ?)";

	public static final String ADD_COMPANY_STAGE = "insert into tracking(stage,company_id) values(?,?)";
	public static final String UPDATE_COMPANY_STAGE = "update tracking set stage=? where id=?";
	public static final String GET_COMPANY_STAGE = "select * from tracking where company_id=?";

	public static final String ADD_COMPANY_STATUS = "insert into pipeline(status,stage,company_id) values(?,?,?)";
	public static final String UPDATE_COMPANY_STATUS = "update pipeline set status=?,stage=? where id=?";
	public static final String GET_COMPANY_STATUS = "select * from pipeline where company_id=?";
	
	public static final String ADD_COMPANY_STATUS2 = "insert into pipeline(status_id,status_name,status_colour,stage_id,stage_name,company_id) values(?,?,?,?,?,?)";
	public static final String UPDATE_COMPANY_STATUS2 = "update pipeline set status_id=?,status_name=?,status_colour=?,stage_id=?,stage_name=? where id=?";
	public static final String GET_COMPANY_STATUS2 = "select * from pipeline where company_id=?";
	
	public static final String ADD_REMARKS = "insert into remarks(date,comments,company_id) values(?,?,?)";
	public static final String UPDATE_REMARKS = "update remarks set date=?,comments=? where id=?";
	public static final String GET_REMARKS="select * from remarks where company_id=?";
	
	public static final String DELETE_COMPANY="delete from company where id=?";
	
	public static final String GET_CP = "SELECT * FROM users_privileges WHERE role LIKE '%Data%'";
	public static final String GET_CP2 = "SELECT * FROM users_privileges WHERE username!='user1'";
	
	public static final String UPDATE_USER= "update users_privileges set privilege_id=?,role=? where id=?";
	public static final String GET_USER= "select * from users_privileges where id=?";
	
	public static final String CHECK_USER = "select * from users where username=?";
	public static final String ADD_USER = "insert into users(username,password,fullname,email_id) values(?,?,?,?)";
	public static final String GET_USERID = "select * from users where username=?";
	public static final String ADD_PRIVILEGE = "insert into users_privileges(user_id,username,privilege_id,role) values(?,?,2,'DataEntry')";
}
