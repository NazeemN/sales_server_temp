package in.fissionlabs.sales.model;

public class HomeRetrieval extends ClientResponse {

	private Home home;

	public HomeRetrieval(int responseCode, String responseMessage, Home home) {
		super(responseCode, responseMessage);
		this.home = home;

	}

	public Home getHome() {
		return home;
	}

	public void setHome(Home home) {
		this.home = home;
	}

}
