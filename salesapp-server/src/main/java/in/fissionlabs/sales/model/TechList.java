package in.fissionlabs.sales.model;

import java.util.List;

public class TechList extends ClientResponse {

	private List<Technology> techstack;

	public TechList(int responseCode, String responseMessage,
			List<Technology> techstack) {
		super(responseCode, responseMessage);
		this.techstack = techstack;
	}

	public List<Technology> getTechstack() {
		return techstack;
	}

	public void setTechstack(List<Technology> techstack) {
		this.techstack = techstack;
	}

}
