package in.fissionlabs.sales.model;

import java.io.Serializable;
import java.util.List;

public class Company extends ClientResponse implements Serializable {

	private static final long serialVersionUID = 2100538509778133957L;
	private int companyId;
	private String companyName;
	private String companyUrl;
	private String addedBy;

	private Profile profile;
	private List<Location> locations;
	private List<Funding> fundings;
	private List<Technology> techstack;
	private List<Job> jobs;
	private List<Competitor> competitors;
	private List<Conection> connections;

	private Pipeline2 pipeline;
	private List<Remark> remarks;

	public Company(int id, String name, String url, String addedBy,
			Profile profile, List<Location> locations, List<Funding> fundings,
			List<Technology> techstack, List<Job> jobs,
			List<Competitor> competitors, List<Conection> connections) {
		super();
		companyId = id;
		companyName = name;
		companyUrl = url;
		this.addedBy = addedBy;
		this.profile = profile;
		this.locations = locations;
		this.fundings = fundings;
		this.techstack = techstack;
		this.jobs = jobs;
		this.competitors = competitors;
		this.connections = connections;
	}

	public Company() {
	};

	public Company(int responseCode, String responseMessage, int id,
			String name, String url, String addedBy, Profile profile,
			List<Location> locations, List<Funding> fundings,
			List<Technology> techstack, List<Job> jobs,
			List<Competitor> competitors, List<Conection> connections) {
		super(responseCode, responseMessage);
		companyId = id;
		companyName = name;
		companyUrl = url;
		this.addedBy = addedBy;
		this.profile = profile;
		this.locations = locations;
		this.fundings = fundings;
		this.techstack = techstack;
		this.jobs = jobs;
		this.competitors = competitors;
		this.connections = connections;
	}

	public Company(int responseCode, String responseMessage, int id,
			String name, String url, String addedBy, Profile profile,
			List<Location> locations, List<Funding> fundings,
			List<Technology> techstack, List<Job> jobs,
			List<Competitor> competitors, List<Conection> connections,
			Pipeline2 pipeline) {
		super(responseCode, responseMessage);
		companyId = id;
		companyName = name;
		companyUrl = url;
		this.addedBy = addedBy;
		this.profile = profile;
		this.locations = locations;
		this.fundings = fundings;
		this.techstack = techstack;
		this.jobs = jobs;
		this.competitors = competitors;
		this.connections = connections;
		this.pipeline = pipeline;
	}

	public Company(int responseCode, String responseMessage, int id,
			String name, String url, String addedBy, Profile profile,
			List<Location> locations, List<Funding> fundings,
			List<Technology> techstack, List<Job> jobs,
			List<Competitor> competitors, List<Conection> connections,
			Pipeline2 pipeline, List<Remark> remarks) {
		super(responseCode, responseMessage);
		companyId = id;
		companyName = name;
		companyUrl = url;
		this.addedBy = addedBy;
		this.profile = profile;
		this.locations = locations;
		this.fundings = fundings;
		this.techstack = techstack;
		this.jobs = jobs;
		this.competitors = competitors;
		this.connections = connections;
		this.pipeline = pipeline;
		this.remarks = remarks;
	}

	public List<Remark> getRemarks() {
		return remarks;
	}

	public void setRemarks(List<Remark> remarks) {
		this.remarks = remarks;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setId(int id) {
		companyId = id;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setName(String name) {
		companyName = name;
	}

	public String getcompanyUrl() {
		return companyUrl;
	}

	public void setUrl(String url) {
		companyUrl = url;
	}

	public String getAddedBy() {
		return addedBy;
	}

	public void setAddedBy(String addedBy) {
		this.addedBy = addedBy;
	}

	public Profile getProfile() {
		return profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	public List<Location> getLocations() {
		return locations;
	}

	public void setLocations(List<Location> locations) {
		this.locations = locations;
	}

	public List<Funding> getFundings() {
		return fundings;
	}

	public void setFundings(List<Funding> fundings) {
		this.fundings = fundings;
	}

	public List<Technology> getTechstack() {
		return techstack;
	}

	public void setTechstack(List<Technology> techstack) {
		this.techstack = techstack;
	}

	public List<Job> getJobs() {
		return jobs;
	}

	public void setJobs(List<Job> jobs) {
		this.jobs = jobs;
	}

	public List<Competitor> getCompetitors() {
		return competitors;
	}

	public void setCompetitors(List<Competitor> competitors) {
		this.competitors = competitors;
	}

	public List<Conection> getConnections() {
		return connections;
	}

	public void setConnections(List<Conection> connections) {
		this.connections = connections;
	}

	public Pipeline2 getPipeline() {
		return pipeline;
	}

	public void setPipeline(Pipeline2 pipeline) {
		this.pipeline = pipeline;
	}

}
