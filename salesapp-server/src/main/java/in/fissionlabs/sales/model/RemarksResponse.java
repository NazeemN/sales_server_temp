package in.fissionlabs.sales.model;

import java.util.List;

public class RemarksResponse extends ClientResponse {

	private List<Remark> remarks;

	public RemarksResponse(int responseCode, String responseMessage,
			List<Remark> remarks) {
		super(responseCode, responseMessage);
		this.remarks = remarks;
	}

	public List<Remark> getRemarks() {
		return remarks;
	}

	public void setRemarks(List<Remark> remarks) {
		this.remarks = remarks;
	}

}
