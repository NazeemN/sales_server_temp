package in.fissionlabs.sales.model;

import java.io.Serializable;

public class Funding implements Serializable {

	private static final long serialVersionUID = -391920039696802327L;
	private int fundingId;
	private String type;
	private String date;
	private String amount;
	private int companyId;

	public Funding(int fundingId, String type, String date, String amount,
			int companyId) {
		super();
		this.fundingId = fundingId;
		this.type = type;
		this.date = date;
		this.amount = amount;
		this.companyId = companyId;
	}

	public int getFundingId() {
		return fundingId;
	}

	public void setFundingId(int fundingId) {
		this.fundingId = fundingId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

}
