package in.fissionlabs.sales.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.internet.MimeBodyPart;

public class Scheduler {

	private String server = null;
	private String port = null;

	public String getServer() {
		return server;
	}

	public void setServer(String server) {
		this.server = server;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public static Session getSession() {

		Scheduler scheduler = new Scheduler();
		scheduler.setPort("587");
		scheduler.setServer("smtp.gmail.com");

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		// Put below to false, if no https is needed
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", scheduler.getServer());
		props.put("mail.smtp.port", scheduler.getPort());

		return Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication("fissionsalesapp@gmail.com",
						"fission123");
			}
		});
	}
	
	
	public static BodyPart getMailPart() throws MessagingException {

		MimeBodyPart descriptionPart = new MimeBodyPart();

		String content = "<font size=\"2\">Please check your calendar</font>";
		descriptionPart.setContent(content, "text/html; charset=utf-8");

		return descriptionPart;
	}
	
	public static BodyPart getMeetingPart(String invitee,String date,String from,String to,String subject,String location) throws Exception {

		BodyPart meetingPart = new MimeBodyPart();

		Calendar cal = Calendar.getInstance();
		
		SimpleDateFormat inFormatter = new SimpleDateFormat("dd/MM/yyyyhh.mm a");
		SimpleDateFormat outFormatter = new SimpleDateFormat("yyyyMMdd'T'HHmmss");
		
		String calendarContent = "BEGIN:VCALENDAR\n" 
				+ "PRODID:-//Google Inc//Google Calendar 70.9054//EN\n" 
				+ "VERSION:2.0\n"
				+ "METHOD:REQUEST\n"
				+ "CALSCALE:GREGORIAN\n"
				+ "BEGIN:VEVENT\n" + "DTSTAMP:"
				+ outFormatter.format(inFormatter.parse(date+from))
				+ "\n"
				+ "DTSTART:"
				+ outFormatter.format(inFormatter.parse(date+from))
				+ "\n"
				+ "DTEND:"
				+ outFormatter.format(inFormatter.parse(date+to))
				+ "\n"
				+ "SUMMARY:"+subject+"\n"
				+ "UID:"+cal.getTimeInMillis()+"@fissionlabs.in"+"\n"
				+ "ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;RSVP=TRUE;CN="+invitee+";"
				+ "X-NUM-GUESTS=0:mailto:"+invitee+"\n"
				+ "ORGANIZER:MAILTO:fissionsalesapp@gmail.com\n"
				+ "LOCATION:"+location+"\n"
				+ "DESCRIPTION:Business Discussion \n"
				+ "SEQUENCE:0\n"
				+ "PRIORITY:5\n"
				+ "CLASS:PUBLIC\n"
				+ "STATUS:CONFIRMED\n"
				+ "TRANSP:OPAQUE\n"
				+ "BEGIN:VALARM\n"
				+ "ACTION:DISPLAY\n"
				+ "DESCRIPTION:REMINDER\n"
				+ "TRIGGER;RELATED=START:-PT00H15M00S\n"
				+ "END:VALARM\n"
				+ "END:VEVENT\n" + "END:VCALENDAR";

		meetingPart.addHeader("Content-Class",
				"urn:content-classes:calendarmessage");
		meetingPart.setContent(calendarContent, "text/calendar");

		return meetingPart;
	}

}
