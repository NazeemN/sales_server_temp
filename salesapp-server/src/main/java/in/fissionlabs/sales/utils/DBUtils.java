package in.fissionlabs.sales.utils;

import in.fissionlabs.sales.dao.SalesDAOImpl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DBUtils {
	
	private static final Logger LOGGER= LoggerFactory.getLogger(DBUtils.class);

	public static Connection getDbConnection() throws Exception {

		Connection connection = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			connection = DriverManager
					.getConnection("jdbc:mysql://localhost:3306/sales?user=sales&password=fission123&useUnicode=true&characterEncoding=utf8&connectionCollation=utf8_general_ci");
		} catch (SQLException e) {
			LOGGER.debug(e.getCause().getMessage());
		}

	//	System.out.println(connection.getCatalog());
		return connection;
	}
	
	public static void closeConnection(Connection con) {
		try {
			if (con != null) {
				con.close();
			}
		} catch (Exception ignore) {

		}
	}

	public static void closeStatement(PreparedStatement stmt) {
		try {
			if (stmt != null) {
				stmt.close();
			}
		} catch (Exception ignore) {

		}
	}

	public static void closeResultSet(ResultSet rset) {
		try {
			if (rset != null) {
				rset.close();
			}
		} catch (Exception ignore) {

		}
	}

	public static void closeResources(PreparedStatement statement,
			Connection connection) throws SQLException {
		DBUtils.closeStatement(statement);
		DBUtils.closeConnection(connection);
	}

	public static void closeResources(ResultSet rset,
			PreparedStatement statement, Connection connection)
			throws SQLException {
		DBUtils.closeResultSet(rset);
		DBUtils.closeStatement(statement);
		DBUtils.closeConnection(connection);
	}

}
