package in.fissionlabs.sales.model;

import java.util.List;

public class CPResponse extends ClientResponse {

	private List<UserNPrivilege> controlPanel;

	public CPResponse(int responseCode, String responseMessage,
			List<UserNPrivilege> controlPanel) {
		super(responseCode, responseMessage);
		this.controlPanel = controlPanel;
	}

	public List<UserNPrivilege> getControlPanel() {
		return controlPanel;
	}

	public void setControlPanel(List<UserNPrivilege> controlPanel) {
		this.controlPanel = controlPanel;
	}

}
