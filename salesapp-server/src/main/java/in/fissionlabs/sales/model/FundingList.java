package in.fissionlabs.sales.model;

import java.util.List;

public class FundingList extends ClientResponse {

	private List<Funding> fundings;

	public FundingList(int responseCode, String responseMessage,
			List<Funding> fundings) {
		super(responseCode, responseMessage);
		this.fundings = fundings;
	}

	public List<Funding> getFundings() {
		return fundings;
	}

	public void setFundings(List<Funding> fundings) {
		this.fundings = fundings;
	}

}
