package in.fissionlabs.sales.model;

import java.util.List;

public class JobList extends ClientResponse {

	private List<Job> jobs;

	public JobList(int responseCode, String responseMessage, List<Job> jobs) {
		super(responseCode, responseMessage);
		this.jobs = jobs;
	}

	public List<Job> getJobs() {
		return jobs;
	}

	public void setJobs(List<Job> jobs) {
		this.jobs = jobs;
	}

}
