package in.fissionlabs.sales.model;

//import java.util.List;

public class ClientResponse {

	private int responseCode;
	private String responseMessage;
	//private List<String> privileges;

	/*public List<String> getPrivileges() {
		return privileges;
	}

	public void setPrivileges(List<String> privileges) {
		this.privileges = privileges;
	}

	public ClientResponse(int responseCode, String responseMessage,
			List<String> privileges) {
		super();
		this.responseCode = responseCode;
		this.responseMessage = responseMessage;
		this.privileges = privileges;
	}*/

	public ClientResponse() {
	}

	public ClientResponse(int responseCode, String responseMessage) {
		this.responseCode = responseCode;
		this.responseMessage = responseMessage;
	}

	public int getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

}
