package in.fissionlabs.sales.model;

public class UserNPrivilege {

	private int id;
	private int userId;
	private String username;
	private int privilegeId;
	private String role;

	public UserNPrivilege(int id, int userId, String username, int privilegeId,
			String role) {
		super();
		this.id = id;
		this.userId = userId;
		this.username = username;
		this.privilegeId = privilegeId;
		this.role = role;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public int getPrivilegeId() {
		return privilegeId;
	}

	public void setPrivilegeId(int privilegeId) {
		this.privilegeId = privilegeId;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

}
