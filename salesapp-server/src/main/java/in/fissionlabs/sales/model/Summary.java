package in.fissionlabs.sales.model;

import java.io.Serializable;

public class Summary implements Serializable {

	private static final long serialVersionUID = 5716000469122820909L;
	private int summaryId;
	private String founder;
	private String foundedYear;
	private String employeeCount;
	private String ceo;
	private String cfo;
	private String vp;
	

	public Summary(String founder, String foundedYear, String employeeCount,
			String ceo, String cfo, String vp,int id) {
		super();
		this.founder = founder;
		this.foundedYear = foundedYear;
		this.employeeCount = employeeCount;
		this.ceo = ceo;
		this.cfo = cfo;
		this.vp = vp;
		summaryId = id;
	}

	public int getSummaryId() {
		return summaryId;
	}

	public void setId(int id) {
		summaryId = id;
	}

	public String getFounder() {
		return founder;
	}

	public void setFounder(String founder) {
		this.founder = founder;
	}

	public String getFoundedYear() {
		return foundedYear;
	}

	public void setFoundedYear(String foundedYear) {
		this.foundedYear = foundedYear;
	}

	public String getEmployeeCount() {
		return employeeCount;
	}

	public void setEmployeeCount(String employeeCount) {
		this.employeeCount = employeeCount;
	}

	public String getCeo() {
		return ceo;
	}

	public void setCeo(String ceo) {
		this.ceo = ceo;
	}

	public String getCfo() {
		return cfo;
	}

	public void setCfo(String cfo) {
		this.cfo = cfo;
	}

	public String getVp() {
		return vp;
	}

	public void setVp(String vp) {
		this.vp = vp;
	}

	@Override
	public String toString() {
		return "Summary [founder=" + founder + ", foundedYear=" + foundedYear
				+ ", employeeCount=" + employeeCount + ", ceo=" + ceo
				+ ", cfo=" + cfo + ", vp=" + vp + "]";
	}

}
