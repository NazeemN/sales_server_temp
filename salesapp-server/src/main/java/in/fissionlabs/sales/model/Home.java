package in.fissionlabs.sales.model;

import java.io.Serializable;

public class Home implements Serializable {

	private static final long serialVersionUID = -8325702074602197210L;
	private int companyId;
	private String name;
	private String url;
	private String addedBy;

	public Home(String name,
			String url, int id, String addedBy) {
		super();
		this.name = name;
		this.url = url;
		companyId = id;
		this.addedBy = addedBy;
	}

	public String getAddedBy() {
		return addedBy;
	}

	public void setAddedBy(String addedBy) {
		this.addedBy = addedBy;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setId(int id) {
		companyId = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}
