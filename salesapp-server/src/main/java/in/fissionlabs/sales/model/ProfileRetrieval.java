package in.fissionlabs.sales.model;

public class ProfileRetrieval extends ClientResponse {

	private Profile profile;

	public ProfileRetrieval(int responseCode, String responseMessage,
			Profile profile) {
		super(responseCode, responseMessage);
		this.profile = profile;
	}

	public Profile getProfile() {
		return profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

}
