package in.fissionlabs.sales.model;

import java.io.Serializable;

public class Competitor implements Serializable {

	private static final long serialVersionUID = -2717677038800878614L;
	private int competitorId;
	private String name;
	private String source;
	private int companyId;

	public Competitor(int competitorId, String name, String source,
			int companyId) {
		super();
		this.competitorId = competitorId;
		this.name = name;
		this.source = source;
		this.companyId = companyId;
	}

	public int getCompetitorId() {
		return competitorId;
	}

	public void setCompetitorId(int competitorId) {
		this.competitorId = competitorId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

}
