package in.fissionlabs.sales.dao;

import java.sql.SQLException;
import java.util.List;

import in.fissionlabs.sales.model.*;

public interface SalesDAO {

	public ClientResponse addCompany(Company company) throws Exception;

	public ClientResponse addCompanyHome(Home home) throws Exception;

	public ClientResponse addCompanyProfile(int company_id, Profile profile)
			throws Exception;

	public void addCompanySummary(int profile_id, Summary summary)
			throws Exception;

	public ClientResponse addCompanyCompetitors(List<Competitor> competitors)
			throws Exception;

	/*
	 * public ClientResponse addCompanyTechStack(int company_id, List<String>
	 * techstack) throws Exception;
	 */

	public ClientResponse addCompanyTechStack(List<Technology> techstack)
			throws Exception;

	public ClientResponse addCompanyFunding(List<Funding> fundings)
			throws Exception;

	public ClientResponse addCompanyJobs(List<Job> jobs) throws Exception;

	public ClientResponse addCompanyLocations(List<Location> locations)
			throws Exception;

	public ClientResponse addCompanyConnections(List<Conection> connections)
			throws Exception;

	public ClientResponse getCompany(int id) throws Exception;

	public ClientResponse getCompanyHome(int id) throws Exception;

	public ClientResponse getCompanyProfile(int id) throws Exception;

	public ClientResponse getCompanyCompetitors(int id) throws Exception;

	// public List<String> getCompanyTechstack(int id) throws Exception;

	public ClientResponse getCompanyTechstack(int id) throws Exception;

	public ClientResponse getCompanyFunding(int id) throws Exception;

	public ClientResponse getCompanyJobs(int id) throws Exception;

	public ClientResponse getCompanyLocations(int id) throws Exception;

	public ClientResponse getCompanyConnections(int id) throws Exception;

	public ClientResponse validateCredentials(User user) throws SQLException;

	public List<String> getPrivileges(String userName) throws Exception;

	public List<Home> getCompanyList(String username) throws Exception;

	public ClientResponse getCompaniesByLocation(String searchString)
			throws Exception;

	public ClientResponse getCompaniesByTechnology(String searchString)
			throws Exception;

	public ClientResponse addCompanyStage(Stage stage) throws Exception;

	public ClientResponse getCompanyStage(int id) throws Exception;

	public boolean validateLogin(User user) throws Exception;

	public ClientResponse validateCredentials2(String username)
			throws SQLException;

	public ClientResponse addCompanyStatus(Pipeline pipeline) throws Exception;
}
