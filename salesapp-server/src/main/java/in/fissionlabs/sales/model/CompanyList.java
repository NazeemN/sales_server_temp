package in.fissionlabs.sales.model;

import java.util.List;

public class CompanyList {

	private List<Home> complete;
	private List<Home> incomplete;

	public List<Home> getComplete() {
		return complete;
	}

	public void setComplete(List<Home> complete) {
		this.complete = complete;
	}

	public List<Home> getIncomplete() {
		return incomplete;
	}

	public void setIncomplete(List<Home> incomplete) {
		this.incomplete = incomplete;
	}

	public CompanyList(List<Home> complete, List<Home> incomplete) {
		super();
		this.complete = complete;
		this.incomplete= incomplete;
	}

}
