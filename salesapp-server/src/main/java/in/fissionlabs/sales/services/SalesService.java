package in.fissionlabs.sales.services;

import java.sql.SQLException;
import java.util.List;

import in.fissionlabs.sales.model.*;

public interface SalesService {

	public ClientResponse addCompany(String companyJson,String username) throws Exception;

	public ClientResponse addCompanyHome(String homeJson) throws Exception;

	public ClientResponse addCompanyProfile(String profileJson)
			throws Exception;

	public ClientResponse addCompanyCompetitors(String competitorsJson)
			throws Exception;

	public ClientResponse addCompanyTechStack(String techstackJson)
			throws Exception;

	public ClientResponse addCompanyFunding(String fundingsJson)
			throws Exception;

	public ClientResponse addCompanyJobs(String jobsJson) throws Exception;

	public ClientResponse addCompanyLocations(String locationsJson)
			throws Exception;

	public ClientResponse addCompanyConnections(String connectionsJson)
			throws Exception;

	public Company getCompany(int id) throws Exception;

	public ClientResponse getCompanyHome(int id) throws Exception;

	public ClientResponse getCompanyProfile(int id) throws Exception;

	public ClientResponse getCompanyCompetitors(int id) throws Exception;

	// public List<String> getCompanyTechstack(int id) throws Exception;
	public ClientResponse getCompanyTechstack(int id) throws Exception;

	public ClientResponse getCompanyFunding(int id) throws Exception;

	public ClientResponse getCompanyJobs(int id) throws Exception;

	public ClientResponse getCompanyLocations(int id) throws Exception;

	public ClientResponse getCompanyConnections(int id) throws Exception;

	public ClientResponse validateCredentials(String userJson) throws Exception;

	public List<Home> getCompanyList(String username) throws Exception;

	public ClientResponse getCompaniesByLocation(String searchString)
			throws Exception;

	public ClientResponse getCompaniesByTechnology(String searchString)
			throws Exception;

	public ClientResponse addCompanyStage(String stageJson) throws Exception;
	
	public ClientResponse getCompanyStage(int id) throws Exception;
	
	public ClientResponse addCompanyStatus(String pipelineJson) throws Exception;
	
	public ClientResponse addCompanyStatus2(String pipelineJson)
			throws Exception;

	public boolean validateLogin(String userJson) throws Exception;

	public ClientResponse validateCredentials2(String username)
			throws SQLException;

	public ClientResponse sendEmail(String meetingJson) throws Exception;

	public ClientResponse addRemarks(String remarksJson) throws Exception;

	public ClientResponse deleteCompany(int id) throws Exception;

	public ClientResponse getDataForControlPanel() throws Exception;

	public ClientResponse updateUser(String userPrivilegeJson) throws Exception;

	public ClientResponse getDataForControlPanel2() throws Exception;

	public ClientResponse addUser(String newUserJson) throws Exception;

	public ClientResponse recoverPassword(String username) throws Exception;

}
