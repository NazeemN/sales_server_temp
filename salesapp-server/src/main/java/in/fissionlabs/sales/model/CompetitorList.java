package in.fissionlabs.sales.model;

import java.util.List;

public class CompetitorList extends ClientResponse {
	private List<Competitor> competitors;

	public CompetitorList(int responseCode, String responseMessage,
			List<Competitor> competitors) {
		super(responseCode, responseMessage);
		this.competitors = competitors;
	}

	public List<Competitor> getCompetitors() {
		return competitors;
	}

	public void setCompetitors(List<Competitor> competitors) {
		this.competitors = competitors;
	}

}
