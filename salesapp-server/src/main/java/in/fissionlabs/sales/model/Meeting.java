package in.fissionlabs.sales.model;

public class Meeting {

	private int id;
	private int company_id;
	private String username;
	private String date;
	private String fromTime;
	private String toTime;
	private String subject;
	private String location;
	private String emailTo;

	public Meeting(int id, int company_id, String username, String date,
			String fromTime, String toTime, String subject, String location,
			String emailTo) {
		super();
		this.id = id;
		this.company_id = company_id;
		this.username = username;
		this.date = date;
		this.fromTime = fromTime;
		this.toTime = toTime;
		this.subject = subject;
		this.location = location;
		this.emailTo = emailTo;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCompany_id() {
		return company_id;
	}

	public void setCompany_id(int company_id) {
		this.company_id = company_id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getFromTime() {
		return fromTime;
	}

	public void setFromTime(String fromTime) {
		this.fromTime = fromTime;
	}

	public String getToTime() {
		return toTime;
	}

	public void setToTime(String toTime) {
		this.toTime = toTime;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getEmailTo() {
		return emailTo;
	}

	public void setEmailTo(String emailTo) {
		this.emailTo = emailTo;
	}

}
