package in.fissionlabs.sales.model;

import java.io.Serializable;
import java.util.List;

public class Profile implements Serializable {

	private static final long serialVersionUID = 52896316987852514L;
	private int profileId;
	private String description;
	private String specialities;
	private Summary summary;
	private int companyId;

	public Profile(int profileId, String description,
			String specialities, Summary summary, int companyId) {
		super();
		this.profileId = profileId;
		this.description = description;
		this.specialities = specialities;
		this.summary = summary;
		this.companyId = companyId;
	}

	public int getProfileId() {
		return profileId;
	}

	public void setProfileId(int profileId) {
		this.profileId = profileId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSpecialities() {
		return specialities;
	}

	public void setSpecialities(String specialities) {
		this.specialities = specialities;
	}

	public Summary getSummary() {
		return summary;
	}

	public void setSummary(Summary summary) {
		this.summary = summary;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

}
