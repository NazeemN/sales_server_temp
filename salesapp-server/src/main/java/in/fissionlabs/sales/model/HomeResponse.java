package in.fissionlabs.sales.model;

public class HomeResponse extends ClientResponse {

	private int companyId;

	public HomeResponse(int responseCode, String responseMessage, int companyId) {
		super(responseCode, responseMessage);
		this.companyId = companyId;

	}

	public int getCompanyId() {
		return companyId;
	}

	public void setHomeId(int homeId) {
		this.companyId = homeId;
	}

}