package in.fissionlabs.sales.model;

import java.util.List;

public class LoginResponse extends ClientResponse {

	private List<String> privileges;
	private CompanyList companyList;

	public CompanyList getCompanyList() {
		return companyList;
	}

	public void setCompanyList(CompanyList companyList) {
		this.companyList = companyList;
	}

	public List<String> getPrivileges() {
		return privileges;
	}

	public void setPrivileges(List<String> privileges) {
		this.privileges = privileges;
	}

	public LoginResponse() {
		super();
	}

	public LoginResponse(int responseCode, String responseMessage,
			List<String> privileges, CompanyList companyList) {
		super(responseCode, responseMessage);
		this.privileges = privileges;
		this.companyList=companyList;
	}

}
