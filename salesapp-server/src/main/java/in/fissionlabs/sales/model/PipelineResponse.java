package in.fissionlabs.sales.model;

public class PipelineResponse extends ClientResponse {

	private Pipeline pipeline;

	public PipelineResponse(int responseCode, String responseMessage,
			Pipeline pipeline) {
		super(responseCode, responseMessage);
		this.pipeline = pipeline;
	}

	public Pipeline getPipeline() {
		return pipeline;
	}

	public void setPipeline(Pipeline pipeline) {
		this.pipeline = pipeline;
	}

}
