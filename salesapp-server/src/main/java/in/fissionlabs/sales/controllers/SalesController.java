package in.fissionlabs.sales.controllers;

import java.sql.SQLException;
import java.util.List;

//import javax.servlet.http.HttpServletResponse;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import in.fissionlabs.sales.dao.SalesDAOImpl;
import in.fissionlabs.sales.model.*;
import in.fissionlabs.sales.services.SalesService;
import in.fissionlabs.sales.utils.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class SalesController {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(SalesController.class);

	@Autowired
	SalesService salesService;

	ClientResponse response = null;
	HttpSession httpSession = null;

	@RequestMapping(value = URI_Constants.LOGIN, method = RequestMethod.POST)
	public @ResponseBody ClientResponse userLogin(HttpServletRequest request,
			HttpServletResponse response3, @RequestBody String userJson)
			throws Exception {
		if (SimpleUtils.isValidSession(httpSession)) {
			return new ClientResponse(200,
					(String) httpSession.getAttribute("User")
							+ " already logged in..!! Please "
							+ "logout and relogin for a diffrent user");
		}

		if (salesService.validateCredentials(userJson).getClass() == LoginResponse2.class) {
			LoginResponse2 response1 = (LoginResponse2) salesService
					.validateCredentials(userJson);
			String userName = response1.getResponseMessage().split(" ")[0];
			httpSession = request.getSession();
			httpSession.setAttribute("User", userName);
			httpSession.setAttribute("Privilege", "DE");
			for (String privilege : response1.getPrivileges()) {
				if (privilege.equalsIgnoreCase("Admin")) {
					httpSession.setAttribute("Privilege", "Admin");
				}
			}

			Cookie cookie = new Cookie("sessionUser",
					(String) httpSession.getAttribute("User"));
			response3.addCookie(cookie);
		}
		return salesService.validateCredentials(userJson);
	}

	/*
	 * @RequestMapping(value = URI_Constants.LOGIN, method = RequestMethod.POST)
	 * public @ResponseBody ClientResponse userLogin(HttpServletRequest request,
	 * HttpServletResponse response3, @RequestBody String username) throws
	 * Exception { if (SimpleUtils.isValidSession(httpSession)) { return new
	 * ClientResponse(200, (String) httpSession.getAttribute("User") +
	 * " already logged in..!!"); }
	 * 
	 * if (salesService.validateCredentials2(username).getClass() ==
	 * LoginResponse2.class) { LoginResponse2 response1 = (LoginResponse2)
	 * salesService .validateCredentials(userJson); String userName =
	 * response1.getResponseMessage().split(" ")[0]; httpSession =
	 * request.getSession(); httpSession.setAttribute("User", username); Cookie
	 * cookie = new Cookie("sessionUser", (String)
	 * httpSession.getAttribute("User")); response3.addCookie(cookie); } if
	 * (SimpleUtils.isValidSession(httpSession)) { return
	 * salesService.validateCredentials2(username); } else return new
	 * ClientResponse(200, "Please Login first!!");
	 * 
	 * if (salesService.validateCredentials2(username).getClass() ==
	 * LoginResponse2.class) { LoginResponse2 response1 = (LoginResponse2)
	 * salesService .validateCredentials(userJson); String userName =
	 * response1.getResponseMessage().split(" ")[0]; httpSession =
	 * request.getSession(); httpSession.setAttribute("User", username); Cookie
	 * cookie = new Cookie("sessionUser", (String)
	 * httpSession.getAttribute("User")); response3.addCookie(cookie); } return
	 * salesService.validateCredentials2(username); }
	 */

	@RequestMapping(value = URI_Constants.LOGIN1, method = RequestMethod.POST)
	public @ResponseBody boolean validateLogin(@RequestBody String userJson)
			throws Exception {

		return salesService.validateLogin(userJson);
	}

	@RequestMapping(value = URI_Constants.ADD_COMPANY, method = RequestMethod.POST)
	public @ResponseBody ClientResponse addCompany(
			@RequestBody String companyJson) throws Exception {
		if (SimpleUtils.isValidSession(httpSession)) {
			String username = (String) httpSession.getAttribute("User");
			return salesService.addCompany(companyJson, username);
		} else
			return new ClientResponse(200, "Please Login first!!");

	}

	/*
	 * @RequestMapping(value = URI_Constants.GET_COMPANYLIST, method =
	 * RequestMethod.GET) public @ResponseBody List<Home>
	 * getCompanyList(@PathVariable("username") String username) throws
	 * Exception {
	 * 
	 * try { String message = salesService.validateCredentials(userJson);
	 * response = new ClientResponse(200, message); } catch (Exception e) {
	 * response = new ClientResponse(500, "Internal server error: " +
	 * e.getCause().getMessage()); }
	 * 
	 * return salesService.getCompanyList(username); }
	 */

	@RequestMapping(value = URI_Constants.ADD_COMPANY_HOME, method = RequestMethod.POST)
	public @ResponseBody ClientResponse addCompanyHome(
			@RequestBody String homeJson) throws Exception {

		if (SimpleUtils.isValidSession(httpSession)) {
			return salesService.addCompanyHome(homeJson);
		} else
			return new ClientResponse(200, "Please Login first!!");
	}

	@RequestMapping(value = URI_Constants.ADD_PROFILE, method = RequestMethod.POST)
	public @ResponseBody ClientResponse addProfile(
			@RequestBody String profileJson) throws Exception {

		if (SimpleUtils.isValidSession(httpSession)) {
			return salesService.addCompanyProfile(profileJson);
		} else
			return new ClientResponse(200, "Please Login first!!");
	}

	@RequestMapping(value = URI_Constants.ADD_COMPETITORS, method = RequestMethod.POST)
	public @ResponseBody ClientResponse addCompetitors(
			@RequestBody String competitorsJson) throws Exception {
		if (SimpleUtils.isValidSession(httpSession)) {
			return salesService.addCompanyCompetitors(competitorsJson);
		} else
			return new ClientResponse(200, "Please Login first!!");
	}

	@RequestMapping(value = URI_Constants.ADD_TECHSTACK, method = RequestMethod.POST)
	public @ResponseBody ClientResponse addTechstack(
			@RequestBody String techstackJson) throws Exception {
		if (SimpleUtils.isValidSession(httpSession)) {
			return salesService.addCompanyTechStack(techstackJson);
		} else
			return new ClientResponse(200, "Please Login first!!");
	}

	@RequestMapping(value = URI_Constants.ADD_FUNDINGS, method = RequestMethod.POST)
	public @ResponseBody ClientResponse addFunding(
			@RequestBody String fundingsJson) throws Exception {
		if (SimpleUtils.isValidSession(httpSession)) {
			return salesService.addCompanyFunding(fundingsJson);
		} else
			return new ClientResponse(200, "Please Login first!!");
	}

	@RequestMapping(value = URI_Constants.ADD_JOBS, method = RequestMethod.POST)
	public @ResponseBody ClientResponse addJobs(@RequestBody String jobsJson)
			throws Exception {
		if (SimpleUtils.isValidSession(httpSession)) {
			return salesService.addCompanyJobs(jobsJson);
		} else
			return new ClientResponse(200, "Please Login first!!");
	}

	@RequestMapping(value = URI_Constants.ADD_LOCATIONS, method = RequestMethod.POST)
	public @ResponseBody ClientResponse addLocations(
			@RequestBody String locationsJson) throws Exception {
		if (SimpleUtils.isValidSession(httpSession)) {
			return salesService.addCompanyLocations(locationsJson);
		} else
			return new ClientResponse(200, "Please Login first!!");
	}

	@RequestMapping(value = URI_Constants.ADD_CONNECTIONS, method = RequestMethod.POST)
	public @ResponseBody ClientResponse addConnections(
			@RequestBody String connectionsJson) throws Exception {
		if (SimpleUtils.isValidSession(httpSession)) {
			return salesService.addCompanyConnections(connectionsJson);
		} else
			return new ClientResponse(200, "Please Login first!!");
	}

	@RequestMapping(value = URI_Constants.GET_COMPANY, method = RequestMethod.GET)
	public @ResponseBody ClientResponse getCompany(@PathVariable("id") int id)
			throws Exception {
		if (SimpleUtils.isValidSession(httpSession)) {
			return salesService.getCompany(id);
		} else
			return new ClientResponse(200, "Please Login first!!");
	}

	@RequestMapping(value = URI_Constants.DELETE_COMPANY, method = RequestMethod.POST)
	public @ResponseBody ClientResponse deleteCompany(@PathVariable("id") int id)
			throws Exception {
		if (SimpleUtils.isValidSession(httpSession)) {
			return salesService.deleteCompany(id);
		} else
			return new ClientResponse(200, "Please Login first!!");
	}

	@RequestMapping(value = URI_Constants.GET_PROFILE, method = RequestMethod.GET)
	public @ResponseBody ClientResponse getProfile(@PathVariable("id") int id)
			throws Exception {
		if (SimpleUtils.isValidSession(httpSession)) {
			return salesService.getCompanyProfile(id);
		} else
			return new ClientResponse(200, "Please Login first!!");
	}

	@RequestMapping(value = URI_Constants.GET_COMPETITORS, method = RequestMethod.GET)
	public @ResponseBody ClientResponse getCompetitors(
			@PathVariable("id") int id) throws Exception {
		if (SimpleUtils.isValidSession(httpSession)) {
			return salesService.getCompanyCompetitors(id);
		} else
			return new ClientResponse(200, "Please Login first!!");
	}

	/*
	 * @RequestMapping(value = URI_Constants.GET_TECHSTACK, method =
	 * RequestMethod.GET) public @ResponseBody List<String>
	 * getTechstack(@PathVariable("id") int id) throws Exception { return
	 * salesService.getCompanyTechstack(id); }
	 */

	@RequestMapping(value = URI_Constants.GET_TECHSTACK, method = RequestMethod.GET)
	public @ResponseBody ClientResponse getTechstack(@PathVariable("id") int id)
			throws Exception {
		if (SimpleUtils.isValidSession(httpSession)) {
			return salesService.getCompanyTechstack(id);
		} else
			return new ClientResponse(200, "Please Login first!!");
	}

	@RequestMapping(value = URI_Constants.GET_FUNDINGS, method = RequestMethod.GET)
	public @ResponseBody ClientResponse getFunding(@PathVariable("id") int id)
			throws Exception {
		if (SimpleUtils.isValidSession(httpSession)) {
			return salesService.getCompanyFunding(id);
		} else
			return new ClientResponse(200, "Please Login first!!");
	}

	@RequestMapping(value = URI_Constants.GET_JOBS, method = RequestMethod.GET)
	public @ResponseBody ClientResponse getJobs(@PathVariable("id") int id)
			throws Exception {
		if (SimpleUtils.isValidSession(httpSession)) {
			return salesService.getCompanyJobs(id);
		} else
			return new ClientResponse(200, "Please Login first!!");
	}

	@RequestMapping(value = URI_Constants.GET_LOCATIONS, method = RequestMethod.GET)
	public @ResponseBody ClientResponse getLocations(@PathVariable("id") int id)
			throws Exception {
		if (SimpleUtils.isValidSession(httpSession)) {
			return salesService.getCompanyLocations(id);
		} else
			return new ClientResponse(200, "Please Login first!!");
	}

	@RequestMapping(value = URI_Constants.GET_CONNECTIONS, method = RequestMethod.GET)
	public @ResponseBody ClientResponse getConnections(
			@PathVariable("id") int id) throws Exception {
		if (SimpleUtils.isValidSession(httpSession)) {
			return salesService.getCompanyConnections(id);
		} else
			return new ClientResponse(200, "Please Login first!!");
	}

	@RequestMapping(value = URI_Constants.GET_COMPANY_HOME, method = RequestMethod.GET)
	public @ResponseBody ClientResponse getHome(@PathVariable("id") int id)
			throws Exception {
		if (SimpleUtils.isValidSession(httpSession)) {
			return salesService.getCompanyHome(id);
		} else
			return new ClientResponse(200, "Please Login first!!");
	}

	@RequestMapping(value = URI_Constants.LOCATION_COMPANYLIST, method = RequestMethod.GET)
	public @ResponseBody ClientResponse searchCompaniesByLocation(
			@PathVariable("searchString") String searchString) throws Exception {
		if (SimpleUtils.isValidSession(httpSession)) {
			return salesService.getCompaniesByLocation(searchString);
		} else
			return new ClientResponse(200, "Please Login first!!");
	}

	@RequestMapping(value = URI_Constants.TECHNOLOGY_COMPANYLIST, method = RequestMethod.GET)
	public @ResponseBody ClientResponse searchCompaniesByTechnology(
			@PathVariable("searchString") String searchString) throws Exception {
		if (SimpleUtils.isValidSession(httpSession)) {
			return salesService.getCompaniesByTechnology(searchString);
		} else
			return new ClientResponse(200, "Please Login first!!");
	}

	@RequestMapping(value = URI_Constants.LOGOUT, method = RequestMethod.POST)
	public @ResponseBody ClientResponse userLogout(HttpServletRequest req,
			HttpServletResponse response3) {
		if (SimpleUtils.isValidSession(httpSession)) {
			httpSession.setAttribute("User", null);
			httpSession.setAttribute("Privilege", null);
			httpSession.invalidate();
			httpSession = null;

			Cookie[] cookies = req.getCookies();
			for (Cookie cookie : cookies) {
				cookie.setMaxAge(0);
				response3.addCookie(cookie);
			}

			return new ClientResponse(200, "Successful Logout");
		}

		return new ClientResponse(200, "Login Please!!");
	}

	@RequestMapping(value = URI_Constants.CHECK_SESSION, method = RequestMethod.GET)
	public @ResponseBody boolean checkSession() {

		return SimpleUtils.isValidSession(httpSession);
	}

	@RequestMapping(value = URI_Constants.REFRESH, method = RequestMethod.GET)
	public @ResponseBody ClientResponse getDataForRefresh() throws Exception {
		if (SimpleUtils.isValidSession(httpSession)) {
			String username = (String) httpSession.getAttribute("User");
			return salesService.validateCredentials2(username);
		} else {
			return new ClientResponse(200, "Please Login first!!");
		}
	}

	/*
	 * @RequestMapping(value = URI_Constants.ADD_STAGE, method =
	 * RequestMethod.POST) public @ResponseBody ClientResponse
	 * addCompanyStage(@RequestBody String stageJson) throws Exception { if
	 * (SimpleUtils.isValidSession(httpSession)) { return
	 * salesService.addCompanyStage(stageJson); } else return new
	 * ClientResponse(200, "Please Login first!!"); }
	 * 
	 * @RequestMapping(value = URI_Constants.GET_STAGE, method =
	 * RequestMethod.GET) public @ResponseBody ClientResponse
	 * getCompanyStage(@PathVariable("id") int id) throws Exception { if
	 * (SimpleUtils.isValidSession(httpSession)) { return
	 * salesService.getCompanyStage(id); } else return new ClientResponse(200,
	 * "Please Login first!!"); }
	 */
	@RequestMapping(value = URI_Constants.ADD_STATUS, method = RequestMethod.POST)
	public @ResponseBody ClientResponse addCompanyStage(
			@RequestBody String pipelineJson) throws Exception {
		if (SimpleUtils.isValidSession(httpSession)) {
			return salesService.addCompanyStatus2(pipelineJson);
		} else
			return new ClientResponse(200, "Please Login first!!");
	}

	@RequestMapping(value = URI_Constants.SEND_MAIL, method = RequestMethod.POST)
	public @ResponseBody ClientResponse sendMail(@RequestBody String meetingJson)
			throws Exception {
		if (SimpleUtils.isValidSession(httpSession)) {
			return salesService.sendEmail(meetingJson);
		} else {
			return new ClientResponse(200, "Please Login first!!");
		}
	}

	@RequestMapping(value = URI_Constants.ADD_REMARKS, method = RequestMethod.POST)
	public @ResponseBody ClientResponse addRemarks(
			@RequestBody String remarksJson) throws Exception {
		if (SimpleUtils.isValidSession(httpSession)) {
			return salesService.addRemarks(remarksJson);
		} else
			return new ClientResponse(200, "Please Login first!!");
	}

	@RequestMapping(value = URI_Constants.CONTROL_PANEL, method = RequestMethod.GET)
	public @ResponseBody ClientResponse getDataForControlPanel()
			throws Exception {
		if (SimpleUtils.isValidSession(httpSession)) {
			if (((String) httpSession.getAttribute("User")).equals("user1")) {
				return salesService.getDataForControlPanel2();
			} else {
				// return salesService.getDataForControlPanel();
				return new ClientResponse(200,
						"Only super admin can view control panel!!");
			}
		} else {
			return new ClientResponse(200, "Please Login first!!");
		}
	}

	@RequestMapping(value = URI_Constants.UPDATE_USER, method = RequestMethod.POST)
	public @ResponseBody ClientResponse updateUser(
			@RequestBody String userPrivilegeJson) throws Exception {
		if (SimpleUtils.isValidSession(httpSession)) {
			// if (httpSession.getAttribute("Privilege").equals("Admin")) {
			if (((String) httpSession.getAttribute("User")).equals("user1")) {
				return salesService.updateUser(userPrivilegeJson);
			} else {
				return new ClientResponse(200,
						"Only super admin can update user roles!!");
			}
		} else
			return new ClientResponse(200, "Please Login first!!");
	}

	@RequestMapping(value = URI_Constants.ADD_USER, method = RequestMethod.POST)
	public @ResponseBody ClientResponse addUser(@RequestBody String newUserJson)
			throws Exception {

		return salesService.addUser(newUserJson);

	}
	
	@RequestMapping(value = URI_Constants.RECOVER_PASSWORD, method = RequestMethod.GET)
	public @ResponseBody ClientResponse recoverForgottenPassword(
			@PathVariable("username") String username) throws Exception {
		return salesService.recoverPassword(username);
	}
}