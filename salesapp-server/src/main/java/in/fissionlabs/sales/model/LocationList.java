package in.fissionlabs.sales.model;

import java.util.List;

public class LocationList extends ClientResponse {

	private List<Location> locations;

	public LocationList(int responseCode, String responseMessage,
			List<Location> locations) {
		super(responseCode, responseMessage);
		this.locations = locations;
	}

	public List<Location> getLocations() {
		return locations;
	}

	public void setLocations(List<Location> locations) {
		this.locations = locations;
	}

}
