package in.fissionlabs.sales.model;

import java.io.Serializable;

public class Technology implements Serializable {

	private static final long serialVersionUID = -1609188271219081014L;
	private int techId;
	private String technology_name;
	private int companyId;

	public Technology(int techId, String technology_name, int companyId) {
		super();
		this.techId = techId;
		this.technology_name = technology_name;
		this.companyId = companyId;
	}

	public int getTechId() {
		return techId;
	}

	public void setTechId(int techId) {
		this.techId = techId;
	}

	public String getTechnology_name() {
		return technology_name;
	}

	public void setTechnology_name(String technology_name) {
		this.technology_name = technology_name;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

}
