package in.fissionlabs.sales.services;

import in.fissionlabs.sales.dao.SalesDAOImpl;
import in.fissionlabs.sales.model.ClientResponse;
import in.fissionlabs.sales.model.Company;
import in.fissionlabs.sales.model.Competitor;
import in.fissionlabs.sales.model.Conection;
import in.fissionlabs.sales.model.Funding;
import in.fissionlabs.sales.model.Home;
import in.fissionlabs.sales.model.Job;
import in.fissionlabs.sales.model.Location;
import in.fissionlabs.sales.model.Meeting;
import in.fissionlabs.sales.model.Pipeline;
import in.fissionlabs.sales.model.Pipeline2;
import in.fissionlabs.sales.model.Profile;
import in.fissionlabs.sales.model.Remark;
import in.fissionlabs.sales.model.Stage;
import in.fissionlabs.sales.model.Technology;
import in.fissionlabs.sales.model.User;
import in.fissionlabs.sales.model.UserNPrivilege;
import in.fissionlabs.sales.utils.Scheduler;
import in.fissionlabs.sales.utils.SimpleUtils;

import java.sql.SQLException;
import java.util.List;

import javax.activation.MailcapCommandMap;
import javax.activation.MimetypesFileTypeMap;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@Service
public class SalesServiceImpl implements SalesService {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(SalesServiceImpl.class);

	@Autowired
	SalesDAOImpl salesDAO;

	@Autowired
	private MailSender mailSender;

	public ClientResponse addCompany(String companyJson, String username)
			throws Exception {
		Gson gson = new Gson();
		Company company = gson.fromJson(companyJson, Company.class);
		if (StringUtils.isBlank(company.getAddedBy())) {
			company.setAddedBy(username);
		}
		return salesDAO.addCompany(company);
	}

	public ClientResponse addCompanyHome(String homeJson) throws Exception {
		Gson gson = new Gson();
		Home home = gson.fromJson(homeJson, Home.class);
		return salesDAO.addCompanyHome(home);
	}

	public ClientResponse addCompanyProfile(String profileJson)
			throws Exception {
		Gson gson = new Gson();
		Profile profile = gson.fromJson(profileJson, Profile.class);
		return salesDAO.addCompanyProfile(profile.getCompanyId(), profile);

	}

	/*
	 * public ClientResponse addCompanySummary(int company_id, String
	 * summaryJson) throws Exception { Gson gson = new Gson(); Summary summary =
	 * gson.fromJson(summaryJson, Summary.class);
	 * salesDAO.addCompanySummary(company_id, summary); }
	 */

	public ClientResponse addCompanyCompetitors(String competitorsJson)
			throws Exception {
		Gson gson = new Gson();
		List<Competitor> competitors = gson.fromJson(competitorsJson,
				new TypeToken<List<Competitor>>() {
				}.getType());
		return salesDAO.addCompanyCompetitors(competitors);
	}

	/*
	 * public ClientResponse addCompanyTechStack(int company_id, String
	 * techstackJson) throws Exception { Gson gson = new Gson(); List<String>
	 * techstack = gson.fromJson(techstackJson, new TypeToken<List<String>>() {
	 * }.getType()); return salesDAO.addCompanyTechStack(company_id, techstack);
	 * }
	 */

	public ClientResponse addCompanyTechStack(String techstackJson)
			throws Exception {
		Gson gson = new Gson();
		List<Technology> techstack = gson.fromJson(techstackJson,
				new TypeToken<List<Technology>>() {
				}.getType());
		return salesDAO.addCompanyTechStack(techstack);
	}

	public ClientResponse addCompanyFunding(String fundingsJson)
			throws Exception {
		Gson gson = new Gson();
		List<Funding> fundings = gson.fromJson(fundingsJson,
				new TypeToken<List<Funding>>() {
				}.getType());
		return salesDAO.addCompanyFunding(fundings);
	}

	public ClientResponse addCompanyJobs(String jobsJson) throws Exception {
		Gson gson = new Gson();
		List<Job> jobs = gson.fromJson(jobsJson, new TypeToken<List<Job>>() {
		}.getType());
		return salesDAO.addCompanyJobs(jobs);
	}

	public ClientResponse addCompanyLocations(String locationsJson)
			throws Exception {
		Gson gson = new Gson();
		List<Location> locations = gson.fromJson(locationsJson,
				new TypeToken<List<Location>>() {
				}.getType());
		return salesDAO.addCompanyLocations(locations);
	}

	public ClientResponse addCompanyConnections(String connectionsJson)
			throws Exception {
		Gson gson = new Gson();
		List<Conection> connections = gson.fromJson(connectionsJson,
				new TypeToken<List<Conection>>() {
				}.getType());
		return salesDAO.addCompanyConnections(connections);
	}

	public Company getCompany(int id) throws Exception {
		return salesDAO.getCompany(id);
	}

	public ClientResponse getCompanyProfile(int id) throws Exception {
		return salesDAO.getCompanyProfile(id);
	}

	public ClientResponse getCompanyCompetitors(int id) throws Exception {
		return salesDAO.getCompanyCompetitors(id);
	}

	/*
	 * public List<String> getCompanyTechstack(int id) throws Exception { return
	 * salesDAO.getCompanyTechstack(id); }
	 */
	public ClientResponse getCompanyTechstack(int id) throws Exception {
		return salesDAO.getCompanyTechstack(id);
	}

	public ClientResponse getCompanyFunding(int id) throws Exception {
		return salesDAO.getCompanyFunding(id);
	}

	public ClientResponse getCompanyJobs(int id) throws Exception {
		return salesDAO.getCompanyJobs(id);
	}

	public ClientResponse getCompanyLocations(int id) throws Exception {
		return salesDAO.getCompanyLocations(id);
	}

	public ClientResponse getCompanyConnections(int id) throws Exception {
		return salesDAO.getCompanyConnections(id);
	}

	public ClientResponse getCompanyHome(int id) throws Exception {

		return salesDAO.getCompanyHome(id);
	}

	public ClientResponse validateCredentials(String userJson) throws Exception {
		Gson gson = new Gson();
		User user = gson.fromJson(userJson, User.class);

		return salesDAO.validateCredentials(user);
		/*
		 * String
		 * encodedPassword=salesDAO.validateCredentials(user.getUserName());
		 * String decodedPassword = new
		 * String(Base64.decodeBase64(encodedPassword)); if(user.getPassword()
		 * !=null && user.getPassword().equals(decodedPassword)) return "true";
		 * else return "false";
		 */
	}

	public List<Home> getCompanyList(String username) throws Exception {
		return salesDAO.getCompanyList(username);
	}

	public ClientResponse getCompaniesByLocation(String searchString)
			throws Exception {
		return salesDAO.getCompaniesByLocation(searchString);
	}

	public ClientResponse getCompaniesByTechnology(String searchString)
			throws Exception {
		return salesDAO.getCompaniesByTechnology(searchString);
	}

	public ClientResponse addCompanyStage(String stageJson) throws Exception {
		Gson gson = new Gson();
		Stage stage = gson.fromJson(stageJson, Stage.class);
		return salesDAO.addCompanyStage(stage);
	}

	public ClientResponse getCompanyStage(int id) throws Exception {
		return salesDAO.getCompanyStage(id);
	}

	public boolean validateLogin(String userJson) throws Exception {
		Gson gson = new Gson();
		User user = gson.fromJson(userJson, User.class);
		return salesDAO.validateLogin(user);
	}

	public ClientResponse validateCredentials2(String username)
			throws SQLException {
		Gson gson = new Gson();
		String userName = gson.fromJson(username, String.class);
		return salesDAO.validateCredentials2(userName);
	}

	public ClientResponse sendEmail(String meetingJson) throws Exception {
		Gson gson = new Gson();
		Meeting meeting = gson.fromJson(meetingJson, Meeting.class);

		if (StringUtils.isNotBlank(meeting.getEmailTo())) {
			if (!SimpleUtils.isValidEmail(meeting.getEmailTo())) {
				return new ClientResponse(200,
						"Please enter a valid email address of recipient or leave the field blank!!");
			}
		} else {
			meeting.setEmailTo("sreekanth.duggineni@fissionlabs.in");
		}

		MimetypesFileTypeMap mimetypes = (MimetypesFileTypeMap) MimetypesFileTypeMap
				.getDefaultFileTypeMap();
		mimetypes.addMimeTypes("text/calendar ics ICS");

		MailcapCommandMap mailcap = (MailcapCommandMap) MailcapCommandMap
				.getDefaultCommandMap();
		mailcap.addMailcap("text/calendar;; x-java-content-handler=com.sun.mail.handlers.text_plain");
		Session session = Scheduler.getSession();
		MimeMessage message = new MimeMessage(session);
		message.setFrom(new InternetAddress("fissionsalesapp@gmail.com",
				"Sales App"));
		message.setSubject("Meeting Scheduler : " + meeting.getSubject());
		message.addRecipient(Message.RecipientType.TO, new InternetAddress(
				meeting.getEmailTo()));

		Multipart multipart = new MimeMultipart("alternative");
		multipart.addBodyPart(Scheduler.getMailPart());
		multipart.addBodyPart(Scheduler.getMeetingPart(meeting.getEmailTo(),
				meeting.getDate(), meeting.getFromTime(), meeting.getToTime(),
				meeting.getSubject(), meeting.getLocation()));

		message.setContent(multipart);

		Transport transport = session.getTransport("smtp");
		transport.connect();
		transport.sendMessage(message, message.getAllRecipients());
		transport.close();

		return new ClientResponse(200, "Meeting info mailed successfully!!");
	}

	public ClientResponse addCompanyStatus(String pipelineJson)
			throws Exception {
		Gson gson = new Gson();
		Pipeline pipeline = gson.fromJson(pipelineJson, Pipeline.class);
		return salesDAO.addCompanyStatus(pipeline);
	}

	public ClientResponse addCompanyStatus2(String pipelineJson)
			throws Exception {
		Gson gson = new Gson();
		Pipeline2 pipeline = gson.fromJson(pipelineJson, Pipeline2.class);
		return salesDAO.addCompanyStatus2(pipeline);
	}

	public ClientResponse addRemarks(String remarksJson) throws Exception {
		Gson gson = new Gson();
		List<Remark> remarks = gson.fromJson(remarksJson,
				new TypeToken<List<Remark>>() {
				}.getType());
		return salesDAO.addRemarks(remarks);
	}

	public ClientResponse deleteCompany(int id) throws Exception {
		return salesDAO.deleteCompany(id);
	}

	public ClientResponse getDataForControlPanel() throws Exception {
		return salesDAO.getDataForControlPanel();
	}

	public ClientResponse updateUser(String userPrivilegeJson) throws Exception {
		Gson gson = new Gson();
		// UserNPrivilege userNPrivilege = gson.fromJson(userPrivilegeJson,
		// UserNPrivilege.class);
		// return salesDAO.updateUser(userNPrivilege);

		List<UserNPrivilege> usersNprivileges = gson.fromJson(
				userPrivilegeJson, new TypeToken<List<UserNPrivilege>>() {
				}.getType());
		return salesDAO.updateUsers(usersNprivileges);
	}

	public ClientResponse getDataForControlPanel2() throws Exception {
		return salesDAO.getDataForControlPanel2();
	}

	@Override
	public ClientResponse addUser(String newUserJson) throws Exception {
		Gson gson = new Gson();
		User user = gson.fromJson(newUserJson, User.class);
		return salesDAO.addUser(user);
	}

	@Override
	public ClientResponse recoverPassword(String username) throws Exception {
		ClientResponse DAOResponse = salesDAO.recoverPassword(username);
		String message = DAOResponse.getResponseMessage();
		String password = null;
		String emailId = null;

		if (DAOResponse.getResponseCode() == 201) {
			String[] stringArray = message.split(" ");
			password = stringArray[1];
			emailId = stringArray[2];

			SimpleMailMessage mail = new SimpleMailMessage();
			mail.setFrom("fissionsalesapp@gmail.com");

			mail.setTo(emailId);
			mail.setSubject("Sales App Password Recovery for " + username);
			mail.setText("Hi " + username + ", your password for SalesApp is "
					+ password);
			mailSender.send(mail);
			return new ClientResponse(200,
					"Your password has been mailed to registered emailId..");
		} else {
			return DAOResponse;
		}
	}

}
