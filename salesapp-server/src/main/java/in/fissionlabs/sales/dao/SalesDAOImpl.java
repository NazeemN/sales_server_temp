package in.fissionlabs.sales.dao;

import in.fissionlabs.sales.model.CPResponse;
import in.fissionlabs.sales.model.ClientResponse;
import in.fissionlabs.sales.model.Company;
import in.fissionlabs.sales.model.CompanyList;
import in.fissionlabs.sales.model.Competitor;
import in.fissionlabs.sales.model.CompetitorList;
import in.fissionlabs.sales.model.Conection;
import in.fissionlabs.sales.model.ConnectionList;
import in.fissionlabs.sales.model.Funding;
import in.fissionlabs.sales.model.FundingList;
import in.fissionlabs.sales.model.Home;
import in.fissionlabs.sales.model.HomeRetrieval;
import in.fissionlabs.sales.model.Job;
import in.fissionlabs.sales.model.JobList;
import in.fissionlabs.sales.model.Location;
import in.fissionlabs.sales.model.LocationList;
import in.fissionlabs.sales.model.LoginResponse2;
import in.fissionlabs.sales.model.Pipeline;
import in.fissionlabs.sales.model.Pipeline2;
import in.fissionlabs.sales.model.PipelineResponse;
import in.fissionlabs.sales.model.PipelineResponse2;
import in.fissionlabs.sales.model.Profile;
import in.fissionlabs.sales.model.ProfileRetrieval;
import in.fissionlabs.sales.model.Remark;
import in.fissionlabs.sales.model.RemarksResponse;
import in.fissionlabs.sales.model.Stage;
import in.fissionlabs.sales.model.Stage2;
import in.fissionlabs.sales.model.StageResponse;
import in.fissionlabs.sales.model.Status;
import in.fissionlabs.sales.model.Summary;
import in.fissionlabs.sales.model.TechList;
import in.fissionlabs.sales.model.Technology;
import in.fissionlabs.sales.model.User;
import in.fissionlabs.sales.model.UserNPrivilege;
import in.fissionlabs.sales.model.UserNPrivilegeResponse;
import in.fissionlabs.sales.utils.DBUtils;
import in.fissionlabs.sales.utils.QueryConstants;
import in.fissionlabs.sales.utils.SimpleUtils;

import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.dbcp.BasicDataSource;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class SalesDAOImpl implements SalesDAO {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(SalesDAOImpl.class);

	@Autowired
	BasicDataSource dataSource;

	String message = null;
	ClientResponse response = null;
	int homeId = 0;

	// String idString = null;

	public ClientResponse addCompany(Company company) throws Exception {
		Company company1 = new Company();
		// int companyId = 0 ;
		ClientResponse response1 = addCompanyHome(new Home(
				company.getCompanyName(), company.getcompanyUrl(),
				company.getCompanyId(), company.getAddedBy()));
		if (response1.getClass() == HomeRetrieval.class) {
			HomeRetrieval response2 = (HomeRetrieval) response1;
			company1.setId(response2.getHome().getCompanyId());
			company1.setName(response2.getHome().getName());
			company1.setUrl(response2.getHome().getUrl());
			company1.setAddedBy(response2.getHome().getAddedBy());
			// companyId = response2.getHome().getCompanyId();
		} else {
			return response1;
		}

		company.getProfile().setCompanyId(company1.getCompanyId());
		ClientResponse response3 = addCompanyProfile(company1.getCompanyId(),
				company.getProfile());
		if (response3.getClass() == ProfileRetrieval.class) {
			ProfileRetrieval response4 = (ProfileRetrieval) response3;
			company1.setProfile(response4.getProfile());
		} else {
			return response3;
		}

		if (!company.getLocations().isEmpty()) {
			for (Location location : company.getLocations()) {
				location.setCompanyId(company1.getCompanyId());
			}
			ClientResponse response5 = addCompanyLocations(company
					.getLocations());
			if (response5.getClass() == LocationList.class) {
				LocationList response6 = (LocationList) response5;
				company1.setLocations(response6.getLocations());
			} else {
				return response5;
			}
		} else {
			company1.setLocations(new ArrayList<Location>());
		}

		if (!company.getJobs().isEmpty()) {
			for (Job job : company.getJobs()) {
				job.setCompanyId(company1.getCompanyId());
			}
			ClientResponse response7 = addCompanyJobs(company.getJobs());
			if (response7.getClass() == JobList.class) {
				JobList response8 = (JobList) response7;
				company1.setJobs(response8.getJobs());
			} else {
				return response7;
			}
		} else {
			company1.setJobs(new ArrayList<Job>());
		}

		if (!company.getTechstack().isEmpty()) {
			for (Technology tech : company.getTechstack()) {
				tech.setCompanyId(company1.getCompanyId());
			}
			ClientResponse response9 = addCompanyTechStack(company
					.getTechstack());
			if (response9.getClass() == TechList.class) {
				TechList response10 = (TechList) response9;
				company1.setTechstack(response10.getTechstack());
			} else {
				return response9;
			}
		} else {
			company1.setTechstack(new ArrayList<Technology>());
		}

		if (!company.getConnections().isEmpty()) {
			for (Conection conection : company.getConnections()) {
				conection.setCompanyId(company1.getCompanyId());
			}
			ClientResponse response11 = addCompanyConnections(company
					.getConnections());
			if (response11.getClass() == ConnectionList.class) {
				ConnectionList response12 = (ConnectionList) response11;
				company1.setConnections(response12.getConnections());
			} else {
				return response11;
			}
		} else {
			company1.setConnections(new ArrayList<Conection>());
		}

		if (!company.getCompetitors().isEmpty()) {
			for (Competitor competitor : company.getCompetitors()) {
				competitor.setCompanyId(company1.getCompanyId());
			}
			ClientResponse response13 = addCompanyCompetitors(company
					.getCompetitors());
			if (response13.getClass() == CompetitorList.class) {
				CompetitorList response14 = (CompetitorList) response13;
				company1.setCompetitors(response14.getCompetitors());
			} else {
				return response13;
			}
		} else {
			company1.setConnections(new ArrayList<Conection>());
		}

		if (!company.getFundings().isEmpty()) {
			for (Funding funding : company.getFundings()) {
				funding.setCompanyId(company1.getCompanyId());
			}
			ClientResponse response15 = addCompanyFunding(company.getFundings());
			if (response15.getClass() == FundingList.class) {
				FundingList response16 = (FundingList) response15;
				company1.setFundings(response16.getFundings());
			} else {
				return response15;
			}
		} else {
			company1.setFundings(new ArrayList<Funding>());
		}

		message = "Company data saved successfully!!";
		return new Company(200, message, company1.getCompanyId(),
				company1.getCompanyName(), company1.getcompanyUrl(),
				company1.getAddedBy(), company1.getProfile(),
				company1.getLocations(), company1.getFundings(),
				company1.getTechstack(), company1.getJobs(),
				company1.getCompetitors(), company1.getConnections());
	}

	public ClientResponse addCompanyHome(Home home) throws Exception {
		if (StringUtils.isBlank(home.getName())
				&& StringUtils.isBlank(home.getUrl())) {
			message = "Please add the basic info(Home)!!";
			response = new ClientResponse(200, message);
			return response;
		}

		if (!SimpleUtils.isValidDomain(home.getUrl())) {
			return new ClientResponse(200, "Please enter a valid companyurl");
		}

		Connection connection = null;
		PreparedStatement pst = null;
		ResultSet rs = null, rs1 = null;
		try {// connection = DBUtils.getDbConnection();
			connection = DBUtils.getDbConnection();

			if (home.getCompanyId() == 0) {
				pst = (PreparedStatement) connection
						.prepareStatement(QueryConstants.CHECK_COMPANY1);
				pst.setString(1, home.getName());
				rs = pst.executeQuery();
				if (rs.next()) {
					return new ClientResponse(200,
							"Data already exists with this company name!!");
				}

				pst = (PreparedStatement) connection
						.prepareStatement(QueryConstants.ADD_COMPANY);
				pst.setString(1, home.getName());
				pst.setString(2, home.getUrl());
				pst.setString(3, home.getAddedBy());
				pst.executeUpdate();

				pst = (PreparedStatement) connection
						.prepareStatement(QueryConstants.CHECK_COMPANY);
				pst.setString(1, home.getName());
				pst.setString(2, home.getUrl());
				pst.setString(3, home.getAddedBy());
				rs1 = pst.executeQuery();

				while (rs1.next()) {
					homeId = rs1.getInt("id");
				}

			} else {
				pst = (PreparedStatement) connection
						.prepareStatement(QueryConstants.UPDATE_COMPANY);
				pst.setString(1, home.getName());
				pst.setString(2, home.getUrl());
				pst.setInt(3, home.getCompanyId());

				pst.executeUpdate();
				homeId = home.getCompanyId();
			}
			// message = "Company saved successfully";
			// response = new HomeResponse(200, message, homeId);
			if (getCompanyHome(homeId).getClass() == HomeRetrieval.class) {
				response = (HomeRetrieval) getCompanyHome(homeId);
				message = "Company saved successfully";
				response.setResponseMessage(message);
			} else {
				response = getCompanyHome(homeId);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			message = "Internal server error: " + e.getCause().getMessage();
			response = new ClientResponse(500, message);
		} catch (Exception e) {
			e.printStackTrace();
			message = "Internal server error: " + e.getCause().getMessage();
			response = new ClientResponse(500, message);
		} finally {
			DBUtils.closeResources(rs, pst, connection);
			DBUtils.closeResultSet(rs1);
			;
		}
		return response;

	}

	public ClientResponse addCompanyProfile(int company_id, Profile profile)
			throws Exception {
		Connection connection = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		int profile_id = 0;

		try {
			connection = DBUtils.getDbConnection();
			if (!SimpleUtils.areAllFieldsEmpty(profile)) {
				if (profile.getProfileId() == 0) {
					pst = (PreparedStatement) connection
							.prepareStatement(QueryConstants.ADD_COMPANY_PROFILE);
					pst.setString(1, profile.getDescription());
					pst.setString(2, profile.getSpecialities());
					pst.setInt(3, company_id);
					pst.executeUpdate();

					pst = (PreparedStatement) connection
							.prepareStatement(QueryConstants.GET_PROFILE_ID);
					pst.setInt(1, company_id);
					rs = pst.executeQuery();
					while (rs.next()) {
						profile_id = rs.getInt("id");
					}
				} else {
					profile_id = profile.getProfileId();
					pst = (PreparedStatement) connection
							.prepareStatement(QueryConstants.UPDATE_COMPANY_PROFILE);
					pst.setString(1, profile.getDescription());
					pst.setString(2, profile.getSpecialities());
					pst.setInt(3, company_id);
					pst.setInt(4, profile.getProfileId());
					pst.executeUpdate();
				}
			}
			addCompanySummary(profile_id, profile.getSummary());
			// message = "Profile saved successfully";
			// response = new ClientResponse(200, message);
			if (getCompanyProfile(profile.getCompanyId()).getClass() == ProfileRetrieval.class) {
				response = (ProfileRetrieval) getCompanyProfile(profile
						.getCompanyId());
				message = "Profile saved successfully";
				response.setResponseMessage(message);
			} else {
				response = getCompanyProfile(profile.getCompanyId());
			}

		} catch (SQLException e) {
			e.printStackTrace();
			message = "Internal server error: " + e.getCause().getMessage();
			response = new ClientResponse(500, message);
		} catch (Exception e) {
			e.printStackTrace();
			message = "Internal server error: " + e.getCause().getMessage();
			response = new ClientResponse(500, message);
		} finally {
			DBUtils.closeResources(rs, pst, connection);
		}
		return response;
	}

	public void addCompanySummary(int profile_id, Summary summary)
			throws Exception {
		Connection connection = null;
		PreparedStatement pst = null;
		// ResultSet rs = null, rs1 = null;
		// try {
		connection = DBUtils.getDbConnection();
		if (!SimpleUtils.areAllFieldsEmpty(summary)) {
			if (summary.getSummaryId() == 0) {
				pst = (PreparedStatement) connection
						.prepareStatement(QueryConstants.ADD_COMPANY_SUMMARY);
				pst.setString(1, summary.getFounder());
				pst.setString(2, summary.getFoundedYear());
				pst.setString(3, summary.getEmployeeCount());
				pst.setString(4, summary.getCeo());
				pst.setString(5, summary.getCfo());
				pst.setString(6, summary.getVp());
				pst.setInt(7, profile_id);
				pst.executeUpdate();

			} else {
				pst = (PreparedStatement) connection
						.prepareStatement(QueryConstants.UPDATE_COMPANY_SUMMARY);
				pst.setString(1, summary.getFounder());
				pst.setString(2, summary.getFoundedYear());
				pst.setString(3, summary.getEmployeeCount());
				pst.setString(4, summary.getCeo());
				pst.setString(5, summary.getCfo());
				pst.setString(6, summary.getVp());
				pst.setInt(7, profile_id);
				pst.setInt(8, summary.getSummaryId());
				pst.executeUpdate();
			}
		}
		/*
		 * // } catch (SQLException e) { e.printStackTrace(); } catch (Exception
		 * e) { e.printStackTrace(); } finally {
		 */
		DBUtils.closeResources(pst, connection);
		// DBUtils.closeResultSet(rs1);
		// }
	}

	public ClientResponse addCompanyStage(Stage stage) throws Exception {
		Connection connection = null;
		PreparedStatement pst = null;
		try {
			connection = DBUtils.getDbConnection();
			if (!SimpleUtils.areAllFieldsEmpty(stage)) {
				if (stage.getId() == 0) {
					pst = (PreparedStatement) connection
							.prepareStatement(QueryConstants.ADD_COMPANY_STAGE);
					pst.setString(1, stage.getStage());
					pst.setInt(2, stage.getCompanyId());
					pst.executeUpdate();
				} else {
					pst = (PreparedStatement) connection
							.prepareStatement(QueryConstants.UPDATE_COMPANY_STAGE);
					pst.setString(1, stage.getStage());
					pst.setInt(2, stage.getId());
					pst.executeUpdate();
				}
			}
			if (getCompanyStage(stage.getCompanyId()).getClass() == StageResponse.class) {
				response = (StageResponse) getCompanyStage(stage.getCompanyId());
				message = "Stage data saved successfully";
				response.setResponseMessage(message);
			} else {
				response = getCompanyStage(stage.getCompanyId());
			}
		} catch (SQLException e) {
			e.printStackTrace();
			message = "Internal server error: " + e.getCause().getMessage();
			response = new ClientResponse(500, message);
		} catch (Exception e) {
			e.printStackTrace();
			message = "Internal server error: " + e.getCause().getMessage();
			response = new ClientResponse(500, message);
		} finally {
			DBUtils.closeResources(pst, connection);
		}
		return response;
	}

	public ClientResponse addCompanyCompetitors(List<Competitor> competitors)
			throws Exception {
		Connection connection = null;
		PreparedStatement pst = null;
		// ResultSet rs = null;

		try {
			connection = DBUtils.getDbConnection();
			for (Competitor competitor : competitors) {
				if (!SimpleUtils.areAllFieldsEmpty(competitor)) {
					if (competitor.getCompetitorId() == 0) {
						pst = (PreparedStatement) connection
								.prepareStatement(QueryConstants.ADD_COMPANY_COMPETITOR);
						pst.setString(1, competitor.getName());
						pst.setString(2, competitor.getSource());
						pst.setInt(3, competitor.getCompanyId());
						pst.executeUpdate();

					} else {

						pst = (PreparedStatement) connection
								.prepareStatement(QueryConstants.UPDATE_COMPANY_COMPETITOR);
						pst.setString(1, competitor.getName());
						pst.setString(2, competitor.getSource());
						pst.setInt(3, competitor.getCompetitorId());
						pst.executeUpdate();
					}
				}
			}
			// message = "Competitors saved successfully";
			// response = new ClientResponse(200, message);

			if (getCompanyCompetitors(competitors.get(0).getCompanyId())
					.getClass() == CompetitorList.class) {
				response = (CompetitorList) getCompanyCompetitors(competitors
						.get(0).getCompanyId());
				message = "Competitors saved successfully";
				response.setResponseMessage(message);
			} else {
				response = getCompanyCompetitors(competitors.get(0)
						.getCompanyId());
			}

		} catch (SQLException e) {
			e.printStackTrace();
			message = "Internal server error: " + e.getCause().getMessage();
			response = new ClientResponse(500, message);
		} catch (Exception e) {
			e.printStackTrace();
			message = "Internal server error: " + e.getCause().getMessage();
			response = new ClientResponse(500, message);
		} finally {
			DBUtils.closeResources(pst, connection);
		}
		return response;

	}

	public ClientResponse addCompanyTechStack(List<Technology> techstack)
			throws Exception {
		Connection connection = null;
		PreparedStatement pst = null;
		// ResultSet rs = null;

		try {
			connection = DBUtils.getDbConnection();
			for (Technology technology : techstack) {
				if (!SimpleUtils.areAllFieldsEmpty(technology)) {
					if (technology.getTechId() == 0) {
						pst = (PreparedStatement) connection
								.prepareStatement(QueryConstants.ADD_COMPANY_TECHSTACK);
						pst.setString(1, technology.getTechnology_name());
						pst.setInt(2, technology.getCompanyId());
						pst.executeUpdate();

					} else {

						pst = (PreparedStatement) connection
								.prepareStatement(QueryConstants.UPDATE_COMPANY_TECHSTACK);
						pst.setString(1, technology.getTechnology_name());
						pst.setInt(2, technology.getTechId());
						pst.executeUpdate();
					}
				}
			}
			// message = "Techstack saved successfully";
			// response = new ClientResponse(200, message);

			if (getCompanyTechstack(techstack.get(0).getCompanyId()).getClass() == TechList.class) {
				response = (TechList) getCompanyTechstack(techstack.get(0)
						.getCompanyId());
				message = "Techstack saved successfully";
				response.setResponseMessage(message);
			} else {
				response = getCompanyTechstack(techstack.get(0).getCompanyId());
			}

		} catch (SQLException e) {
			e.printStackTrace();
			message = "Internal server error: " + e.getCause().getMessage();
			response = new ClientResponse(500, message);
		} catch (Exception e) {
			e.printStackTrace();
			message = "Internal server error: " + e.getCause().getMessage();
			response = new ClientResponse(500, message);
		} finally {
			DBUtils.closeResources(pst, connection);
		}
		return response;

	}

	public ClientResponse addCompanyFunding(List<Funding> fundings)
			throws Exception {
		Connection connection = null;
		PreparedStatement pst = null;
		// ResultSet rs = null;

		try {
			connection = DBUtils.getDbConnection();
			for (Funding funding : fundings) {
				if (!SimpleUtils.areAllFieldsEmpty(funding)) {
					if (funding.getFundingId() == 0) {
						pst = (PreparedStatement) connection
								.prepareStatement(QueryConstants.ADD_COMPANY_FUNDING);
						pst.setString(1, funding.getType());
						pst.setString(2, funding.getDate());
						pst.setString(3, funding.getAmount());
						pst.setInt(4, funding.getCompanyId());
						pst.executeUpdate();

					} else {

						pst = (PreparedStatement) connection
								.prepareStatement(QueryConstants.UPDATE_COMPANY_FUNDING);
						pst.setString(1, funding.getType());
						pst.setString(2, funding.getDate());
						pst.setString(3, funding.getAmount());
						pst.setInt(4, funding.getFundingId());
						pst.executeUpdate();
					}
				}
			}
			// message = "Funding saved successfully";
			// response = new ClientResponse(200, message);
			if (getCompanyFunding(fundings.get(0).getCompanyId()).getClass() == FundingList.class) {
				response = (FundingList) getCompanyFunding(fundings.get(0)
						.getCompanyId());
				message = "Funding saved successfully";
				response.setResponseMessage(message);
			} else {
				response = getCompanyFunding(fundings.get(0).getCompanyId());
			}

		} catch (SQLException e) {
			e.printStackTrace();
			message = "Internal server error: " + e.getCause().getMessage();
			response = new ClientResponse(500, message);
		} catch (Exception e) {
			e.printStackTrace();
			message = "Internal server error: " + e.getCause().getMessage();
			response = new ClientResponse(500, message);
		} finally {
			DBUtils.closeResources(pst, connection);
		}
		return response;

	}

	public ClientResponse addCompanyJobs(List<Job> jobs) throws Exception {

		Connection connection = null;
		PreparedStatement pst = null;
		// ResultSet rs = null;

		try {
			connection = DBUtils.getDbConnection();
			for (Job job : jobs) {
				if (!SimpleUtils.areAllFieldsEmpty(job)) {
					if (job.getJobId() == 0) {

						pst = (PreparedStatement) connection
								.prepareStatement(QueryConstants.ADD_COMPANY_JOB);
						pst.setString(1, job.getTitle());
						pst.setString(2, job.getDescription());
						pst.setInt(3, job.getCompanyId());
						pst.executeUpdate();
					} else {
						pst = (PreparedStatement) connection
								.prepareStatement(QueryConstants.UPDATE_COMPANY_JOB);
						pst.setString(1, job.getTitle());
						pst.setString(2, job.getDescription());
						pst.setInt(3, job.getJobId());
						pst.executeUpdate();
					}
				}
			}

			// message = "Jobs saved successfully";
			// response = new ClientResponse(200, message);

			if (getCompanyJobs(jobs.get(0).getCompanyId()).getClass() == JobList.class) {
				response = (JobList) getCompanyJobs(jobs.get(0).getCompanyId());
				message = "Jobs saved successfully";
				response.setResponseMessage(message);
			} else {
				response = getCompanyJobs(jobs.get(0).getCompanyId());
			}

		} catch (SQLException e) {
			e.printStackTrace();
			message = "Internal server error: " + e.getCause().getMessage();
			response = new ClientResponse(500, message);
		} catch (Exception e) {
			e.printStackTrace();
			message = "Internal server error: " + e.getCause().getMessage();
			response = new ClientResponse(500, message);
		} finally {
			DBUtils.closeResources(pst, connection);
		}
		return response;

	}

	public ClientResponse addCompanyLocations(List<Location> locations)
			throws Exception {

		Connection connection = null;
		PreparedStatement pst = null;
		// ResultSet rs = null;

		try {
			connection = DBUtils.getDbConnection();
			for (Location location : locations) {
				if (!SimpleUtils.areAllFieldsEmpty(location)) {
					if (location.getLocationId() == 0) {
						pst = (PreparedStatement) connection
								.prepareStatement(QueryConstants.ADD_COMPANY_LOCATION);
						pst.setString(1, location.getIsHeadQuarters());
						/*
						 * pst.setString(2, location.getAddress_line1());
						 * pst.setString(3, location.getAddress_line2());
						 * pst.setString(4, location.getStreet());
						 */
						pst.setString(2, location.getAddress());
						pst.setString(3, location.getCity());
						pst.setString(4, location.getState());
						pst.setString(5, location.getCountry());
						pst.setString(6, location.getZip());
						pst.setInt(7, location.getCompanyId());
						pst.executeUpdate();
					} else {
						pst = (PreparedStatement) connection
								.prepareStatement(QueryConstants.UPDATE_COMPANY_LOCATION);
						pst.setString(1, location.getIsHeadQuarters());
						/*
						 * pst.setString(2, location.getAddress_line1());
						 * pst.setString(3, location.getAddress_line2());
						 * pst.setString(4, location.getStreet());
						 */
						pst.setString(2, location.getAddress());
						pst.setString(3, location.getCity());
						pst.setString(4, location.getState());
						pst.setString(5, location.getCountry());
						pst.setString(6, location.getZip());
						pst.setInt(7, location.getLocationId());
						pst.executeUpdate();

					}
				}
			}
			// message = "Locations saved successfully";
			// response = new ClientResponse(200, message);

			if (getCompanyLocations(locations.get(0).getCompanyId()).getClass() == LocationList.class) {
				response = (LocationList) getCompanyLocations(locations.get(0)
						.getCompanyId());
				message = "Locations saved successfully";
				response.setResponseMessage(message);
			} else {
				response = getCompanyLocations(locations.get(0).getCompanyId());
			}

		} catch (SQLException e) {
			e.printStackTrace();
			message = "Internal server error: " + e.getCause().getMessage();
			response = new ClientResponse(500, message);
		} catch (Exception e) {
			e.printStackTrace();
			message = "Internal server error: " + e.getCause().getMessage();
			response = new ClientResponse(500, message);
		} finally {
			DBUtils.closeResources(pst, connection);
		}
		return response;

	}

	public ClientResponse addCompanyConnections(List<Conection> connections)
			throws Exception {
		Connection connection = null;
		PreparedStatement pst = null;
		// ResultSet rs = null;

		try {
			connection = DBUtils.getDbConnection();
			for (Conection conection : connections) {
				if (!SimpleUtils.areAllFieldsEmpty(conection)) {
					if (conection.getConnectionId() == 0) {
						pst = (PreparedStatement) connection
								.prepareStatement(QueryConstants.ADD_COMPANY_CONNECTION);
						pst.setString(1, conection.getConnectedFrom());
						pst.setString(2, conection.getConnectedTo());
						pst.setString(3, conection.getConnectedThrough());
						pst.setString(4, conection.getLevel());
						pst.setInt(5, conection.getCompanyId());
						pst.executeUpdate();
					} else {
						pst = (PreparedStatement) connection
								.prepareStatement(QueryConstants.UPDATE_COMPANY_CONNECTION);
						pst.setString(1, conection.getConnectedFrom());
						pst.setString(2, conection.getConnectedTo());
						pst.setString(3, conection.getConnectedThrough());
						pst.setString(4, conection.getLevel());
						pst.setInt(5, conection.getConnectionId());
						pst.executeUpdate();
					}
				}
			}
			// message = "Connections saved successfully";
			// response = new ClientResponse(200, message);

			if (getCompanyConnections(connections.get(0).getCompanyId())
					.getClass() == ConnectionList.class) {
				response = (ConnectionList) getCompanyConnections(connections
						.get(0).getCompanyId());
				message = "Connections saved successfully";
				response.setResponseMessage(message);
			} else {
				response = getCompanyConnections(connections.get(0)
						.getCompanyId());
			}

		} catch (SQLException e) {
			e.printStackTrace();
			message = "Internal server error: " + e.getCause().getMessage();
			response = new ClientResponse(500, message);
		} catch (Exception e) {
			e.printStackTrace();
			message = "Internal server error: " + e.getCause().getMessage();
			response = new ClientResponse(500, message);
		} finally {
			DBUtils.closeResources(pst, connection);
		}
		return response;

	}

	public Company getCompany(int id) throws Exception {
		Connection connection = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		Company company = null;
		Summary summary = null;
		Profile profile = null;
		// int profile_id;
		List<Location> locations = null;
		List<Funding> fundings = null;
		List<Technology> techstack = null;
		List<Job> jobs = null;
		List<Competitor> competitors = null;
		List<Conection> connections = null;
		Pipeline2 pipeline = null;
		List<Remark> remarks = null;

		try {
			connection = DBUtils.getDbConnection();

			pst = (PreparedStatement) connection
					.prepareStatement(QueryConstants.GET_COMPANY_SUMMARY);
			pst.setInt(1, id);
			rs = pst.executeQuery();
			while (rs.next()) {
				summary = new Summary(rs.getString("founded_by"),
						rs.getString("founded_year"),
						rs.getString("employees_count"), rs.getString("ceo"),
						rs.getString("cfo"), rs.getString("vp"),
						rs.getInt("id"));
			}

			pst = (PreparedStatement) connection
					.prepareStatement(QueryConstants.GET_COMPANY_PROFILE);
			pst.setInt(1, id);
			rs = pst.executeQuery();
			while (rs.next()) {
				profile = new Profile(rs.getInt("id"),
						rs.getString("description"),
						rs.getString("specialities"), summary,
						rs.getInt("company_id"));
			}

			competitors = new ArrayList<Competitor>();
			pst = (PreparedStatement) connection
					.prepareStatement(QueryConstants.GET_COMPANY_COMPETITORS);
			pst.setInt(1, id);
			rs = pst.executeQuery();
			while (rs.next()) {
				competitors.add(new Competitor(rs.getInt("id"), rs
						.getString("name"), rs.getString("source"), rs
						.getInt("company_id")));
			}

			locations = new ArrayList<Location>();
			pst = (PreparedStatement) connection
					.prepareStatement(QueryConstants.GET_COMPANY_LOCATIONS);
			pst.setInt(1, id);
			rs = pst.executeQuery();
			while (rs.next()) {
				locations.add(new Location(rs.getInt("id"), rs
						.getString("is_head_quarters"),
						rs.getString("address"), rs.getString("city"), rs
								.getString("state"), rs.getString("country"),
						rs.getString("zip"), rs.getInt("company_id")));
			}

			fundings = new ArrayList<Funding>();
			pst = (PreparedStatement) connection
					.prepareStatement(QueryConstants.GET_COMPANY_FUNDINGS);
			pst.setInt(1, id);
			rs = pst.executeQuery();
			while (rs.next()) {
				fundings.add(new Funding(rs.getInt("id"), rs.getString("type"),
						rs.getString("date"), rs.getString("amount"), rs
								.getInt("company_id")));
			}

			techstack = new ArrayList<Technology>();
			pst = (PreparedStatement) connection
					.prepareStatement(QueryConstants.GET_COMPANY_TECHSTACK);
			pst.setInt(1, id);
			rs = pst.executeQuery();
			while (rs.next()) {
				techstack
						.add(new Technology(rs.getInt("id"), rs
								.getString("technology_name"), rs
								.getInt("company_id")));
			}

			jobs = new ArrayList<Job>();
			pst = (PreparedStatement) connection
					.prepareStatement(QueryConstants.GET_COMPANY_JOBS);
			pst.setInt(1, id);
			rs = pst.executeQuery();
			while (rs.next()) {
				jobs.add(new Job(rs.getInt("id"), rs.getString("title"), rs
						.getString("description"), rs.getInt("company_id")));
			}

			connections = new ArrayList<Conection>();
			pst = (PreparedStatement) connection
					.prepareStatement(QueryConstants.GET_COMPANY_CONNECTIONS);
			pst.setInt(1, id);
			rs = pst.executeQuery();
			while (rs.next()) {
				connections.add(new Conection(rs.getInt("id"), rs
						.getString("connected_from"), rs
						.getString("connected_to"), rs
						.getString("connected_through"), rs.getString("level"),
						rs.getInt("company_id")));
			}

			pst = (PreparedStatement) connection
					.prepareStatement(QueryConstants.GET_COMPANY_STATUS2);
			pst.setInt(1, id);
			rs = pst.executeQuery();
			while (rs.next()) {
				pipeline = new Pipeline2(rs.getInt("id"), new Status(
						rs.getInt("status_id"), rs.getString("status_name"),
						rs.getString("status_colour")), new Stage2(
						rs.getInt("stage_id"), rs.getString("stage_name")),
						rs.getInt("company_id"));
			}

			remarks = new ArrayList<Remark>();
			pst = (PreparedStatement) connection
					.prepareStatement(QueryConstants.GET_REMARKS);
			pst.setInt(1, id);
			rs = pst.executeQuery();
			while (rs.next()) {
				remarks.add(new Remark(rs.getInt("id"), rs.getString("date"),
						rs.getString("comments"), rs.getInt("company_id")));
			}

			pst = (PreparedStatement) connection
					.prepareStatement(QueryConstants.GET_COMPANY_HOME);
			pst.setInt(1, id);
			rs = pst.executeQuery();

			message = "Company complete data retrieved successfully";
			while (rs.next()) {
				company = new Company(200, message, id, rs.getString("name"),
						rs.getString("url"), rs.getString("added_by"), profile,
						locations, fundings, techstack, jobs, competitors,
						connections, pipeline, remarks);
			}

		} catch (SQLException e) {
			message = "Internal server error: " + e.getCause().getMessage();
			response = new ClientResponse(500, message);
		} catch (Exception e) {
			message = "Internal server error: " + e.getCause().getMessage();
			response = new ClientResponse(500, message);
		} finally {
			DBUtils.closeResources(rs, pst, connection);
		}

		return company;

	}

	public ClientResponse getCompanyProfile(int id) throws Exception {
		Connection connection = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		Summary summary = null;
		Profile profile = null;
		try {
			connection = DBUtils.getDbConnection();
			pst = (PreparedStatement) connection
					.prepareStatement(QueryConstants.GET_COMPANY_SUMMARY);
			pst.setInt(1, id);
			rs = pst.executeQuery();
			while (rs.next()) {
				summary = new Summary(rs.getString("founded_by"),
						rs.getString("founded_year"),
						rs.getString("employees_count"), rs.getString("ceo"),
						rs.getString("cfo"), rs.getString("vp"),
						rs.getInt("id"));
			}

			pst = (PreparedStatement) connection
					.prepareStatement(QueryConstants.GET_COMPANY_PROFILE);
			pst.setInt(1, id);
			rs = pst.executeQuery();
			while (rs.next()) {
				profile = new Profile(rs.getInt("id"),
						rs.getString("description"),
						rs.getString("specialities"), summary,
						rs.getInt("company_id"));
			}
			message = "Company profile data retrieved successfully";
			response = new ProfileRetrieval(200, message, profile);
		} catch (SQLException e) {
			message = "Internal server error: " + e.getCause().getMessage();
			response = new ClientResponse(500, message);
		} catch (Exception e) {
			message = "Internal server error: " + e.getCause().getMessage();
			response = new ClientResponse(500, message);
		} finally {
			DBUtils.closeResources(rs, pst, connection);
		}
		return response;
	}

	public ClientResponse getCompanyCompetitors(int id) throws Exception {
		Connection connection = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<Competitor> competitors = new ArrayList<Competitor>();
		try {
			connection = DBUtils.getDbConnection();
			pst = (PreparedStatement) connection
					.prepareStatement(QueryConstants.GET_COMPANY_COMPETITORS);
			pst.setInt(1, id);
			rs = pst.executeQuery();
			while (rs.next()) {
				competitors.add(new Competitor(rs.getInt("id"), rs
						.getString("name"), rs.getString("source"), rs
						.getInt("company_id")));
			}
			message = "Company competitors data retrieved successfully";
			response = new CompetitorList(200, message, competitors);
		} catch (SQLException e) {
			message = "Internal server error: " + e.getCause().getMessage();
			response = new ClientResponse(500, message);
		} catch (Exception e) {
			message = "Internal server error: " + e.getCause().getMessage();
			response = new ClientResponse(500, message);
		} finally {
			DBUtils.closeResources(rs, pst, connection);
		}

		return response;
	}

	/*
	 * public List<String> getCompanyTechstack(int id) throws Exception {
	 * Connection connection = null; PreparedStatement pst = null; ResultSet rs
	 * = null; List<String> techstack = new ArrayList<String>(); connection =
	 * dataSource.getConnection();
	 * 
	 * pst = (PreparedStatement) connection
	 * .prepareStatement(QueryConstants.GET_COMPANY_TECHSTACK); pst.setInt(1,
	 * id); rs = pst.executeQuery(); while (rs.next()) {
	 * techstack.add(rs.getString("technology_name")); }
	 * 
	 * return techstack; }
	 */

	public ClientResponse getCompanyTechstack(int id) throws Exception {
		Connection connection = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<Technology> techstack = new ArrayList<Technology>();
		try {
			connection = DBUtils.getDbConnection();

			pst = (PreparedStatement) connection
					.prepareStatement(QueryConstants.GET_COMPANY_TECHSTACK);
			pst.setInt(1, id);
			rs = pst.executeQuery();
			while (rs.next()) {
				techstack
						.add(new Technology(rs.getInt("id"), rs
								.getString("technology_name"), rs
								.getInt("company_id")));
			}
			message = "Company techstack data retrieved successfully";
			response = new TechList(200, message, techstack);
		} catch (SQLException e) {
			message = "Internal server error: " + e.getCause().getMessage();
			response = new ClientResponse(500, message);
		} catch (Exception e) {
			message = "Internal server error: " + e.getCause().getMessage();
			response = new ClientResponse(500, message);
		} finally {
			DBUtils.closeResources(rs, pst, connection);
		}

		return response;
	}

	public ClientResponse getCompanyFunding(int id) throws Exception {
		Connection connection = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<Funding> fundings = new ArrayList<Funding>();
		try {

			connection = DBUtils.getDbConnection();
			pst = (PreparedStatement) connection
					.prepareStatement(QueryConstants.GET_COMPANY_FUNDINGS);
			pst.setInt(1, id);
			rs = pst.executeQuery();
			while (rs.next()) {
				fundings.add(new Funding(rs.getInt("id"), rs.getString("type"),
						rs.getString("date"), rs.getString("amount"), rs
								.getInt("company_id")));
			}

			message = "Company funding data retrieved successfully";
			response = new FundingList(200, message, fundings);
		} catch (SQLException e) {
			message = "Internal server error: " + e.getCause().getMessage();
			response = new ClientResponse(500, message);
		} catch (Exception e) {
			message = "Internal server error: " + e.getCause().getMessage();
			response = new ClientResponse(500, message);
		} finally {
			DBUtils.closeResources(rs, pst, connection);
		}

		return response;

	}

	public ClientResponse getCompanyJobs(int id) throws Exception {
		Connection connection = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<Job> jobs = new ArrayList<Job>();
		try {

			connection = DBUtils.getDbConnection();
			pst = (PreparedStatement) connection
					.prepareStatement(QueryConstants.GET_COMPANY_JOBS);
			pst.setInt(1, id);
			rs = pst.executeQuery();
			while (rs.next()) {
				jobs.add(new Job(rs.getInt("id"), rs.getString("title"), rs
						.getString("description"), rs.getInt("company_id")));
			}
			message = "Company jobs data retrieved successfully";
			response = new JobList(200, message, jobs);
		} catch (SQLException e) {
			message = "Internal server error: " + e.getCause().getMessage();
			response = new ClientResponse(500, message);
		} catch (Exception e) {
			message = "Internal server error: " + e.getCause().getMessage();
			response = new ClientResponse(500, message);
		} finally {
			DBUtils.closeResources(rs, pst, connection);
		}

		return response;
	}

	public ClientResponse getCompanyLocations(int id) throws Exception {
		Connection connection = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<Location> locations = new ArrayList<Location>();
		try {
			connection = DBUtils.getDbConnection();
			pst = (PreparedStatement) connection
					.prepareStatement(QueryConstants.GET_COMPANY_LOCATIONS);
			pst.setInt(1, id);
			rs = pst.executeQuery();
			while (rs.next()) {
				locations.add(new Location(rs.getInt("id"), rs
						.getString("is_head_quarters"),
						rs.getString("address"), rs.getString("city"), rs
								.getString("state"), rs.getString("country"),
						rs.getString("zip"), rs.getInt("company_id")));
			}
			message = "Company locations data retrieved successfully";
			response = new LocationList(200, message, locations);
		} catch (SQLException e) {
			message = "Internal server error: " + e.getCause().getMessage();
			response = new ClientResponse(500, message);
		} catch (Exception e) {
			message = "Internal server error: " + e.getCause().getMessage();
			response = new ClientResponse(500, message);
		} finally {
			DBUtils.closeResources(rs, pst, connection);
		}

		return response;
	}

	public ClientResponse getCompanyConnections(int id) throws Exception {
		Connection connection = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<Conection> connections = new ArrayList<Conection>();
		try {
			connection = DBUtils.getDbConnection();
			pst = (PreparedStatement) connection
					.prepareStatement(QueryConstants.GET_COMPANY_CONNECTIONS);
			pst.setInt(1, id);
			rs = pst.executeQuery();
			while (rs.next()) {
				connections.add(new Conection(rs.getInt("id"), rs
						.getString("connected_from"), rs
						.getString("connected_to"), rs
						.getString("connected_through"), rs.getString("level"),
						rs.getInt("company_id")));
			}
			message = "Company connections data retrieved successfully";
			response = new ConnectionList(200, message, connections);
		} catch (SQLException e) {
			message = "Internal server error: " + e.getCause().getMessage();
			response = new ClientResponse(500, message);
		} catch (Exception e) {
			message = "Internal server error: " + e.getCause().getMessage();
			response = new ClientResponse(500, message);
		} finally {
			DBUtils.closeResources(rs, pst, connection);
		}

		return response;
	}

	public ClientResponse getCompanyHome(int id) throws Exception {
		Connection connection = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		Home home = null;
		try {
			connection = DBUtils.getDbConnection();

			pst = (PreparedStatement) connection
					.prepareStatement(QueryConstants.GET_COMPANY_HOME);
			pst.setInt(1, id);
			rs = pst.executeQuery();
			while (rs.next()) {
				home = new Home(rs.getString("name"), rs.getString("url"), id,
						rs.getString("added_by"));
			}

			message = "Company data retrieved successfully";
			response = new HomeRetrieval(200, message, home);

		} catch (SQLException e) {
			message = "Internal server error: " + e.getCause().getMessage();
			response = new ClientResponse(500, message);
		} catch (Exception e) {
			message = "Internal server error: " + e.getCause().getMessage();
			response = new ClientResponse(500, message);
		} finally {
			DBUtils.closeResources(rs, pst, connection);
		}
		return response;
	}

	public ClientResponse getCompanyStage(int id) throws Exception {
		Connection connection = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		Stage stage = null;

		try {
			connection = DBUtils.getDbConnection();
			pst = (PreparedStatement) connection
					.prepareStatement(QueryConstants.GET_COMPANY_STAGE);
			pst.setInt(1, id);
			rs = pst.executeQuery();
			while (rs.next()) {
				stage = new Stage(rs.getInt("id"), rs.getString("stage"),
						rs.getInt("company_id"));
			}
			message = "Company stage data retrieved successfully";
			response = new StageResponse(200, message, stage);
		} catch (SQLException e) {
			message = "Internal server error: " + e.getCause().getMessage();
			response = new ClientResponse(500, message);
		} catch (Exception e) {
			message = "Internal server error: " + e.getCause().getMessage();
			response = new ClientResponse(500, message);
		} finally {
			DBUtils.closeResources(rs, pst, connection);
		}

		return response;
	}

	public ClientResponse validateCredentials(User user) throws SQLException {
		Connection connection = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String encodedPassword = null;
		// LoginResponse loginResponse = null;
		LoginResponse2 loginResponse = null;
		// LoginResponse3 loginResponse = null;
		try {

			connection = DBUtils.getDbConnection();
			pst = (PreparedStatement) connection
					.prepareStatement(QueryConstants.GET_PASSWORD);
			pst.setString(1, user.getUserName());
			rs = pst.executeQuery();

			while (rs.next()) {
				encodedPassword = rs.getString("password");
			}
			LOGGER.debug("DAO encoded password" + encodedPassword);
			if (encodedPassword != null) {
				String decodedPassword = new String(
						Base64.decodeBase64(encodedPassword));
				if (decodedPassword.equals(user.getPassword())) {
					message = user.getUserName();
					/*
					 * loginResponse = new LoginResponse(200, message,
					 * getPrivileges
					 * (user.getUserName()),getCompleteAndIncompleteList
					 * (user.getUserName()));
					 */

					loginResponse = new LoginResponse2(200, message,
							getPrivileges(user.getUserName()),
							getCompanyList(user.getUserName()));
					// getcompleteDataList(String username)

					/*
					 * loginResponse = new LoginResponse3(200, message,
					 * getcompleteDataList(user.getUserName()));
					 */

				} else {
					message = "Incorrect password";
					return new ClientResponse(200, message);
				}
			} else {
				message = "Invalid user";
				return new ClientResponse(200, message);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			message = "Internal server error: " + e.getCause().getMessage();
			return new ClientResponse(500, message);
		} catch (Exception e) {
			e.printStackTrace();
			message = "Internal server error: " + e.getCause().getMessage();
			return new ClientResponse(500, message);
		} finally {
			DBUtils.closeResources(rs, pst, connection);
		}
		return loginResponse;
	}

	public List<String> getPrivileges(String userName) throws Exception {
		Connection connection = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<String> privileges = null;
		connection = DBUtils.getDbConnection();
		pst = (PreparedStatement) connection
				.prepareStatement(QueryConstants.GET_ROLES);
		pst.setString(1, userName);
		rs = pst.executeQuery();

		privileges = new ArrayList<String>();
		while (rs.next()) {
			privileges.add(rs.getString("role"));
		}

		return privileges;
	}

	public List<Home> getCompanyList(String username) throws Exception {
		Connection connection = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<Home> companyList = null;
		List<Integer> privileges = null;
		connection = DBUtils.getDbConnection();
		pst = (PreparedStatement) connection
				.prepareStatement(QueryConstants.GET_PRIVILEGE_ID);
		pst.setString(1, username);
		rs = pst.executeQuery();

		privileges = new ArrayList<Integer>();
		while (rs.next()) {
			privileges.add(rs.getInt("privilege_id"));
		}
		if (privileges.contains(1)) {
			pst = (PreparedStatement) connection
					.prepareStatement(QueryConstants.GET_COMPLETE_COMPANYLIST);
			rs = pst.executeQuery();

			companyList = new ArrayList<Home>();
			while (rs.next()) {
				companyList.add(new Home(rs.getString("name"), rs
						.getString("url"), rs.getInt("id"), rs
						.getString("added_by")));
			}

		} else {
			pst = (PreparedStatement) connection
					.prepareStatement(QueryConstants.GET_COMPANYLIST);
			pst.setString(1, username);
			rs = pst.executeQuery();

			companyList = new ArrayList<Home>();
			while (rs.next()) {
				companyList.add(new Home(rs.getString("name"), rs
						.getString("url"), rs.getInt("id"), rs
						.getString("added_by")));
			}
		}
		return companyList;
	}

	private CompanyList getCompleteAndIncompleteList(String userName)
			throws Exception {
		// /CompanyList companyList=null;
		List<Home> completeList = null;
		List<Home> incompleteList = null;

		Connection connection = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		// List<Home> companyList = null;
		connection = DBUtils.getDbConnection();
		pst = (PreparedStatement) connection
				.prepareStatement(QueryConstants.GET_COMPANYLIST);
		pst.setString(1, userName);
		rs = pst.executeQuery();
		completeList = new ArrayList<Home>();
		incompleteList = new ArrayList<Home>();
		while (rs.next()) {
			if (companyDataIsComplete(rs.getInt("id"))) {
				completeList.add(new Home(rs.getString("name"), rs
						.getString("url"), rs.getInt("id"), rs
						.getString("added_by")));
			} else {
				incompleteList.add(new Home(rs.getString("name"), rs
						.getString("url"), rs.getInt("id"), rs
						.getString("added_by")));
			}
		}

		return new CompanyList(completeList, incompleteList);

	}

	public boolean companyDataIsComplete(int id) throws Exception {
		boolean isDataComplete = false;
		Connection connection = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		connection = DBUtils.getDbConnection();
		pst = (PreparedStatement) connection
				.prepareStatement(QueryConstants.GET_COMPANY_PROFILE);
		pst.setInt(1, id);
		rs = pst.executeQuery();
		if (rs.next()) {
			pst = (PreparedStatement) connection
					.prepareStatement(QueryConstants.GET_COMPANY_LOCATIONS);
			pst.setInt(1, id);
			rs = pst.executeQuery();
			if (rs.next()) {
				pst = (PreparedStatement) connection
						.prepareStatement(QueryConstants.GET_COMPANY_TECHSTACK);
				pst.setInt(1, id);
				rs = pst.executeQuery();
				if (rs.next()) {
					pst = (PreparedStatement) connection
							.prepareStatement(QueryConstants.GET_COMPANY_JOBS);
					pst.setInt(1, id);
					rs = pst.executeQuery();
					if (rs.next()) {
						pst = (PreparedStatement) connection
								.prepareStatement(QueryConstants.GET_COMPANY_COMPETITORS);
						pst.setInt(1, id);
						rs = pst.executeQuery();
						if (rs.next()) {
							pst = (PreparedStatement) connection
									.prepareStatement(QueryConstants.GET_COMPANY_CONNECTIONS);
							pst.setInt(1, id);
							rs = pst.executeQuery();
							if (rs.next()) {
								pst = (PreparedStatement) connection
										.prepareStatement(QueryConstants.GET_COMPANY_FUNDINGS);
								pst.setInt(1, id);
								rs = pst.executeQuery();
								if (rs.next()) {
									isDataComplete = true;
								}
							}

						}
					}
				}
			}
		}
		return isDataComplete;
	}

	public List<Company> getcompleteDataList(String username) throws Exception {
		Connection connection = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<Company> companyList = null;
		connection = DBUtils.getDbConnection();
		pst = (PreparedStatement) connection
				.prepareStatement(QueryConstants.GET_COMPANYLIST);
		pst.setString(1, username);
		rs = pst.executeQuery();

		companyList = new ArrayList<Company>();
		while (rs.next()) {
			companyList.add(getCompany(rs.getInt("id")));
		}

		return companyList;
	}

	public ClientResponse getCompaniesByLocation(String searchString)
			throws Exception {
		Connection connection = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<Home> companiesByLocation = null;
		String queryString = "%" + searchString + "%";

		connection = DBUtils.getDbConnection();
		pst = (PreparedStatement) connection
				.prepareStatement(QueryConstants.LOCATION_COMPANYLIST);
		for (int i = 1; i < 5; i++) {
			pst.setString(i, queryString);
		}
		rs = pst.executeQuery();

		companiesByLocation = new ArrayList<Home>();
		while (rs.next()) {
			companiesByLocation.add(new Home(rs.getString("name"), rs
					.getString("url"), rs.getInt("id"), rs
					.getString("added_by")));
		}
		return new LoginResponse2(200, null, null, companiesByLocation);
	}

	public ClientResponse getCompaniesByTechnology(String searchString)
			throws Exception {
		Connection connection = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<Home> companiesByTechnology = null;
		String queryString = "%" + searchString + "%";

		connection = DBUtils.getDbConnection();
		pst = (PreparedStatement) connection
				.prepareStatement(QueryConstants.TECHNOLOGY_COMPANYLIST);
		pst.setString(1, queryString);

		rs = pst.executeQuery();

		companiesByTechnology = new ArrayList<Home>();
		while (rs.next()) {
			companiesByTechnology.add(new Home(rs.getString("name"), rs
					.getString("url"), rs.getInt("id"), rs
					.getString("added_by")));
		}

		return new LoginResponse2(200, null, null, companiesByTechnology);
	}

	public boolean validateLogin(User user) throws Exception {
		Connection connection = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String encodedPassword = null;
		try {

			connection = DBUtils.getDbConnection();
			pst = (PreparedStatement) connection
					.prepareStatement(QueryConstants.GET_PASSWORD);
			pst.setString(1, user.getUserName());
			rs = pst.executeQuery();

			while (rs.next()) {
				encodedPassword = rs.getString("password");
			}
			if (encodedPassword != null) {
				String decodedPassword = new String(
						Base64.decodeBase64(encodedPassword));
				if (decodedPassword.equals(user.getPassword())) {
					return true;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
			// message = "Internal server error: " + e.getCause().getMessage();
		} catch (Exception e) {
			e.printStackTrace();
			// message = "Internal server error: " + e.getCause().getMessage();
		} finally {
			DBUtils.closeResources(rs, pst, connection);
		}

		return false;
	}

	public ClientResponse validateCredentials2(String username)
			throws SQLException {
		Connection connection = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String encodedPassword = null;
		// LoginResponse loginResponse = null;
		LoginResponse2 loginResponse = null;
		// LoginResponse3 loginResponse = null;
		try {

			connection = DBUtils.getDbConnection();
			pst = (PreparedStatement) connection
					.prepareStatement(QueryConstants.GET_PASSWORD);
			pst.setString(1, username);
			rs = pst.executeQuery();

			while (rs.next()) {
				encodedPassword = rs.getString("password");
			}
			if (encodedPassword != null) {
				/*
				 * String decodedPassword = new String(
				 * Base64.decodeBase64(encodedPassword)); if
				 * (decodedPassword.equals(user.getPassword())) {
				 */
				message = username;
				/*
				 * loginResponse = new LoginResponse(200, message, getPrivileges
				 * (user.getUserName()),getCompleteAndIncompleteList
				 * (user.getUserName()));
				 */

				loginResponse = new LoginResponse2(200, message,
						getPrivileges(username), getCompanyList(username));
				// getcompleteDataList(String username)

				/*
				 * loginResponse = new LoginResponse3(200, message,
				 * getcompleteDataList(user.getUserName()));
				 */
				/*
				 * } else { message = "Incorrect password"; return new
				 * ClientResponse(200, message); }
				 */
			} else {
				message = "Invalid user";
				return new ClientResponse(200, message);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			message = "Internal server error: " + e.getCause().getMessage();
			return new ClientResponse(500, message);
		} catch (Exception e) {
			e.printStackTrace();
			message = "Internal server error: " + e.getCause().getMessage();
			return new ClientResponse(500, message);
		} finally {
			DBUtils.closeResources(rs, pst, connection);
		}
		return loginResponse;
	}

	public ClientResponse addCompanyStatus(Pipeline pipeline) throws Exception {
		Connection connection = null;
		PreparedStatement pst = null;
		try {
			connection = DBUtils.getDbConnection();
			if (StringUtils.isBlank(pipeline.getStage())
					&& StringUtils.isBlank(pipeline.getStatus())) {
				return new ClientResponse(200,
						"Please fill info for atleast one field");
			}
			if (!SimpleUtils.areAllFieldsEmpty(pipeline)) {
				if (pipeline.getId() == 0) {
					pst = (PreparedStatement) connection
							.prepareStatement(QueryConstants.ADD_COMPANY_STATUS);
					pst.setString(1, pipeline.getStatus());
					pst.setString(2, pipeline.getStage());
					pst.setInt(3, pipeline.getCompanyId());
					pst.executeUpdate();
				} else {
					pst = (PreparedStatement) connection
							.prepareStatement(QueryConstants.UPDATE_COMPANY_STATUS);
					pst.setString(1, pipeline.getStatus());
					pst.setString(2, pipeline.getStage());
					pst.setInt(3, pipeline.getId());
					pst.executeUpdate();
				}
			}

			if (getCompanyStatus(pipeline.getCompanyId()).getClass() == PipelineResponse.class) {
				response = (PipelineResponse) getCompanyStatus(pipeline
						.getCompanyId());
				message = "Company status data saved succesfully!!";
				response.setResponseMessage(message);
			} else {
				response = getCompanyStatus(pipeline.getCompanyId());
			}

		} catch (SQLException e) {
			e.printStackTrace();
			message = "Internal server error: " + e.getCause().getMessage();
			response = new ClientResponse(500, message);
		} catch (Exception e) {
			e.printStackTrace();
			message = "Internal server error: " + e.getCause().getMessage();
			response = new ClientResponse(500, message);
		} finally {
			DBUtils.closeResources(pst, connection);
		}
		return response;
	}

	public ClientResponse getCompanyStatus(int id) throws Exception {
		Connection connection = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		Pipeline pipeline = null;

		try {
			connection = DBUtils.getDbConnection();
			pst = (PreparedStatement) connection
					.prepareStatement(QueryConstants.GET_COMPANY_STATUS);
			pst.setInt(1, id);
			rs = pst.executeQuery();
			while (rs.next()) {
				pipeline = new Pipeline(rs.getInt("id"),
						rs.getString("status"), rs.getString("stage"),
						rs.getInt("company_id"));
			}
			message = "Company status data retrieved successfully";
			response = new PipelineResponse(200, message, pipeline);
		} catch (SQLException e) {
			message = "Internal server error: " + e.getCause().getMessage();
			response = new ClientResponse(500, message);
		} catch (Exception e) {
			message = "Internal server error: " + e.getCause().getMessage();
			response = new ClientResponse(500, message);
		} finally {
			DBUtils.closeResources(rs, pst, connection);
		}

		return response;
	}

	public ClientResponse addCompanyStatus2(Pipeline2 pipeline)
			throws SQLException {
		Connection connection = null;
		PreparedStatement pst = null;
		try {
			connection = DBUtils.getDbConnection();
			if (StringUtils.isBlank(pipeline.getStage().getName())
					&& StringUtils.isBlank(pipeline.getStatus().getName())) {
				return new ClientResponse(200,
						"Please fill info for atleast one field");
			}
			if (pipeline.getId() == 0) {
				pst = (PreparedStatement) connection
						.prepareStatement(QueryConstants.ADD_COMPANY_STATUS2);
				pst.setInt(1, pipeline.getStatus().getId());
				pst.setString(2, pipeline.getStatus().getName());
				pst.setString(3, pipeline.getStatus().getColour());
				pst.setInt(4, pipeline.getStage().getId());
				pst.setString(5, pipeline.getStage().getName());
				pst.setInt(6, pipeline.getCompanyId());
				pst.executeUpdate();
			} else {
				pst = (PreparedStatement) connection
						.prepareStatement(QueryConstants.UPDATE_COMPANY_STATUS2);
				pst.setInt(1, pipeline.getStatus().getId());
				pst.setString(2, pipeline.getStatus().getName());
				pst.setString(3, pipeline.getStatus().getColour());
				pst.setInt(4, pipeline.getStage().getId());
				pst.setString(5, pipeline.getStage().getName());
				pst.setInt(6, pipeline.getId());
				pst.executeUpdate();
			}

			if (getCompanyStatus2(pipeline.getCompanyId()).getClass() == PipelineResponse2.class) {
				response = (PipelineResponse2) getCompanyStatus2(pipeline
						.getCompanyId());
				message = "Company status data saved succesfully!!";
				response.setResponseMessage(message);
			} else {
				response = getCompanyStatus2(pipeline.getCompanyId());
			}

		} catch (SQLException e) {
			e.printStackTrace();
			message = "Internal server error: " + e.getCause().getMessage();
			response = new ClientResponse(500, message);
		} catch (Exception e) {
			e.printStackTrace();
			message = "Internal server error: " + e.getCause().getMessage();
			response = new ClientResponse(500, message);
		} finally {
			DBUtils.closeResources(pst, connection);
		}
		return response;
	}

	public ClientResponse getCompanyStatus2(int id) throws Exception {
		Connection connection = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		Pipeline2 pipeline = null;

		try {
			connection = DBUtils.getDbConnection();
			pst = (PreparedStatement) connection
					.prepareStatement(QueryConstants.GET_COMPANY_STATUS2);
			pst.setInt(1, id);
			rs = pst.executeQuery();
			while (rs.next()) {
				pipeline = new Pipeline2(rs.getInt("id"), new Status(
						rs.getInt("status_id"), rs.getString("status_name"),
						rs.getString("status_colour")), new Stage2(
						rs.getInt("stage_id"), rs.getString("stage_name")),
						rs.getInt("company_id"));
				/*
				 * pipeline = new Pipeline(rs.getInt("id"),
				 * rs.getString("status"), rs.getString("stage"),
				 * rs.getInt("company_id"));
				 */
			}
			message = "Company status data retrieved successfully";
			response = new PipelineResponse2(200, message, pipeline);
		} catch (SQLException e) {
			message = "Internal server error: " + e.getCause().getMessage();
			response = new ClientResponse(500, message);
		} catch (Exception e) {
			message = "Internal server error: " + e.getCause().getMessage();
			response = new ClientResponse(500, message);
		} finally {
			DBUtils.closeResources(rs, pst, connection);
		}

		return response;
	}

	public ClientResponse addRemarks(List<Remark> remarks) throws Exception {
		Connection connection = null;
		PreparedStatement pst = null;
		// ResultSet rs = null;

		try {
			connection = DBUtils.getDbConnection();
			for (Remark remark : remarks) {
				if (!SimpleUtils.areAllFieldsEmpty(remark)) {
					if (remark.getId() == 0) {
						pst = (PreparedStatement) connection
								.prepareStatement(QueryConstants.ADD_REMARKS);
						pst.setString(1, remark.getDate());
						pst.setString(2, remark.getComments());
						pst.setInt(3, remark.getCompanyId());
						pst.executeUpdate();
					} else {
						pst = (PreparedStatement) connection
								.prepareStatement(QueryConstants.UPDATE_REMARKS);
						pst.setString(1, remark.getDate());
						pst.setString(2, remark.getComments());
						pst.setInt(3, remark.getId());
						pst.executeUpdate();

					}
				}
			}

			if (getRemarks(remarks.get(0).getCompanyId()).getClass() == RemarksResponse.class) {
				response = (RemarksResponse) getRemarks(remarks.get(0)
						.getCompanyId());
				message = "Remarks saved successfully";
				response.setResponseMessage(message);
			} else {
				response = getRemarks(remarks.get(0).getCompanyId());
			}

		} catch (SQLException e) {
			e.printStackTrace();
			message = "Internal server error: " + e.getCause().getMessage();
			response = new ClientResponse(500, message);
		} catch (Exception e) {
			e.printStackTrace();
			message = "Internal server error: " + e.getCause().getMessage();
			response = new ClientResponse(500, message);
		} finally {
			DBUtils.closeResources(pst, connection);
		}
		return response;
	}

	public ClientResponse getRemarks(int id) throws Exception {
		Connection connection = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<Remark> remarks = new ArrayList<Remark>();
		try {
			connection = DBUtils.getDbConnection();
			pst = (PreparedStatement) connection
					.prepareStatement(QueryConstants.GET_REMARKS);
			pst.setInt(1, id);
			rs = pst.executeQuery();
			while (rs.next()) {
				remarks.add(new Remark(rs.getInt("id"), rs.getString("date"),
						rs.getString("comments"), rs.getInt("company_id")));
			}
			message = "Remarks data retrieved successfully";
			response = new RemarksResponse(200, message, remarks);
		} catch (SQLException e) {
			message = "Internal server error: " + e.getCause().getMessage();
			response = new ClientResponse(500, message);
		} catch (Exception e) {
			message = "Internal server error: " + e.getCause().getMessage();
			response = new ClientResponse(500, message);
		} finally {
			DBUtils.closeResources(rs, pst, connection);
		}
		return response;
	}

	public ClientResponse deleteCompany(int id) throws Exception {

		if (id == 0) {
			return new ClientResponse(200,
					"Please use a valid company id to delete the company");
		}

		Connection connection = null;
		PreparedStatement pst = null;
		try {
			connection = DBUtils.getDbConnection();
			pst = (PreparedStatement) connection
					.prepareStatement(QueryConstants.DELETE_COMPANY);
			pst.setInt(1, id);
			pst.executeUpdate();

			message = "Company removed successfully";
			response = new ClientResponse(200, message);

		} catch (SQLException e) {
			message = "Internal server error: " + e.getCause().getMessage();
			response = new ClientResponse(500, message);
		} catch (Exception e) {
			message = "Internal server error: " + e.getCause().getMessage();
			response = new ClientResponse(500, message);
		} finally {
			DBUtils.closeResources(pst, connection);
		}
		return response;
	}

	public ClientResponse getDataForControlPanel() throws Exception {
		Connection connection = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<UserNPrivilege> controlPanel = null;

		try {
			connection = DBUtils.getDbConnection();
			pst = (PreparedStatement) connection
					.prepareStatement(QueryConstants.GET_CP);
			rs = pst.executeQuery();

			controlPanel = new ArrayList<UserNPrivilege>();

			while (rs.next()) {
				controlPanel.add(new UserNPrivilege(rs.getInt("id"), rs
						.getInt("user_id"), rs.getString("username"), rs
						.getInt("privilege_id"), rs.getString("role")));
			}

			message = "Control Panel data retrieved successfully";
			response = new CPResponse(200, message, controlPanel);

		} catch (SQLException e) {
			message = "Internal server error: " + e.getCause().getMessage();
			response = new ClientResponse(500, message);
		} catch (Exception e) {
			message = "Internal server error: " + e.getCause().getMessage();
			response = new ClientResponse(500, message);
		} finally {
			DBUtils.closeResources(rs, pst, connection);
		}
		return response;
	}

	public ClientResponse getDataForControlPanel2() throws Exception {
		Connection connection = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<UserNPrivilege> controlPanel = null;

		try {
			connection = DBUtils.getDbConnection();
			pst = (PreparedStatement) connection
					.prepareStatement(QueryConstants.GET_CP2);
			rs = pst.executeQuery();

			controlPanel = new ArrayList<UserNPrivilege>();

			while (rs.next()) {
				controlPanel.add(new UserNPrivilege(rs.getInt("id"), rs
						.getInt("user_id"), rs.getString("username"), rs
						.getInt("privilege_id"), rs.getString("role")));
			}

			message = "Control Panel data retrieved successfully";
			response = new CPResponse(200, message, controlPanel);

		} catch (SQLException e) {
			message = "Internal server error: " + e.getCause().getMessage();
			response = new ClientResponse(500, message);
		} catch (Exception e) {
			message = "Internal server error: " + e.getCause().getMessage();
			response = new ClientResponse(500, message);
		} finally {
			DBUtils.closeResources(rs, pst, connection);
		}
		return response;
	}

	public ClientResponse updateUser(UserNPrivilege userNPrivilege)
			throws Exception {
		Connection connection = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		UserNPrivilege user = null;
		try {
			connection = DBUtils.getDbConnection();
			pst = (PreparedStatement) connection
					.prepareStatement(QueryConstants.UPDATE_USER);

			pst.setInt(1, userNPrivilege.getPrivilegeId());
			pst.setString(2, userNPrivilege.getRole());
			pst.setInt(3, userNPrivilege.getId());
			pst.executeUpdate();

			pst = (PreparedStatement) connection
					.prepareStatement(QueryConstants.GET_USER);
			pst.setInt(1, userNPrivilege.getId());
			rs = pst.executeQuery();

			while (rs.next()) {
				user = new UserNPrivilege(rs.getInt("id"),
						rs.getInt("user_id"), rs.getString("username"),
						rs.getInt("privilege_id"), rs.getString("role"));
			}

			message = userNPrivilege.getUsername()
					+ " data updated successfully";
			response = new UserNPrivilegeResponse(200, message, user);

		} catch (SQLException e) {
			message = "Internal server error: " + e.getCause().getMessage();
			response = new ClientResponse(500, message);
		} catch (Exception e) {
			message = "Internal server error: " + e.getCause().getMessage();
			response = new ClientResponse(500, message);
		} finally {
			DBUtils.closeResources(rs, pst, connection);
		}
		return response;
	}

	public ClientResponse updateUsers(List<UserNPrivilege> usersNprivileges)
			throws Exception {
		Connection connection = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<UserNPrivilege> controlPanel = null;
		try {
			connection = DBUtils.getDbConnection();
			for (UserNPrivilege userNPrivilege : usersNprivileges) {

				pst = (PreparedStatement) connection
						.prepareStatement(QueryConstants.UPDATE_USER);

				pst.setInt(1, userNPrivilege.getPrivilegeId());
				pst.setString(2, userNPrivilege.getRole());
				pst.setInt(3, userNPrivilege.getId());
				pst.executeUpdate();
			}

			pst = (PreparedStatement) connection
					.prepareStatement(QueryConstants.GET_CP2);
			rs = pst.executeQuery();
			controlPanel = new ArrayList<UserNPrivilege>();
			while (rs.next()) {
				controlPanel.add(new UserNPrivilege(rs.getInt("id"), rs
						.getInt("user_id"), rs.getString("username"), rs
						.getInt("privilege_id"), rs.getString("role")));
			}

			message = "Data updated successfully";
			response = new CPResponse(200, message, controlPanel);

		} catch (SQLException e) {
			message = "Internal server error: " + e.getCause().getMessage();
			response = new ClientResponse(500, message);
		} catch (Exception e) {
			message = "Internal server error: " + e.getCause().getMessage();
			response = new ClientResponse(500, message);
		} finally {
			DBUtils.closeResources(rs, pst, connection);
		}
		return response;
	}

	public ClientResponse addUser(User user) throws Exception {
		Connection connection = null;
		PreparedStatement pst = null;
		ResultSet rs = null, rs1 = null;

		if (StringUtils.isBlank(user.getUsername())) {
			return new ClientResponse(200,
					"Usename can not be blank..Please retry!!");
		}

		if (!SimpleUtils.isValidUsername(user.getUsername())) {
			return new ClientResponse(200,
					"username must be of length 6 to 14 and can have only alphabets,numbers,-and_");
		}

		try {
			connection = DBUtils.getDbConnection();
			pst = (PreparedStatement) connection
					.prepareStatement(QueryConstants.CHECK_USER);
			pst.setString(1, user.getUsername());
			rs = pst.executeQuery();
			if (rs.next()) {
				return new ClientResponse(200,
						"username already exists..Please retry!!");
			}

			if (StringUtils.isBlank(user.getPassword())
					|| StringUtils.isBlank(user.getPassword2())) {
				return new ClientResponse(200,
						"Passwords can not be blank..Please retry!!");
			}

			if (!user.getPassword().equals(user.getPassword2())) {
				return new ClientResponse(200,
						"Passwords dont match..Please retry!!");
			}

			if (!SimpleUtils.isValidEmail(user.getEmailId())) {
				return new ClientResponse(200,
						"Please register with a valid email id!!");
			}
			String encodedPassword = new String(Base64.encodeBase64(user
					.getPassword().getBytes(Charset.forName("UTF-8"))));
			pst = (PreparedStatement) connection
					.prepareStatement(QueryConstants.ADD_USER);
			pst.setString(1, user.getUsername());
			pst.setString(2, encodedPassword);
			pst.setString(3, user.getFullname());
			pst.setString(4, user.getEmailId());
			pst.executeUpdate();
			int user_id = 0;
			pst = (PreparedStatement) connection
					.prepareStatement(QueryConstants.GET_USERID);
			pst.setString(1, user.getUsername());
			rs1 = pst.executeQuery();
			while (rs1.next()) {
				user_id = rs1.getInt("id");
			}
			pst = (PreparedStatement) connection
					.prepareStatement(QueryConstants.ADD_PRIVILEGE);
			pst.setInt(1, user_id);
			pst.setString(2, user.getUsername());
			pst.executeUpdate();

			message = user.getUsername() + " registered successfully";
			response = new ClientResponse(200, message);

		} catch (SQLException e) {
			message = "Internal server error: " + e.getCause().getMessage();
			response = new ClientResponse(500, message);
		} catch (Exception e) {
			message = "Internal server error: " + e.getCause().getMessage();
			response = new ClientResponse(500, message);
		} finally {
			DBUtils.closeResources(rs, pst, connection);
			DBUtils.closeResultSet(rs1);
		}
		return response;
	}

	public ClientResponse recoverPassword(String username) throws Exception {

		Connection connection = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String encodedPasword = null;
		String decodedPassword = null;
		String emailId=null;

		if (StringUtils.isBlank(username)) {
			return new ClientResponse(200,
					"Usename can not be blank..Please retry!!");
		}

		try {
			connection = DBUtils.getDbConnection();
			pst = (PreparedStatement) connection
					.prepareStatement(QueryConstants.CHECK_USER);
			pst.setString(1, username);
			rs = pst.executeQuery();
			while (rs.next()) {
				encodedPasword = rs.getString("password");
				emailId=rs.getString("email_id");
			}

			if (StringUtils.isBlank(encodedPasword)) {
				return new ClientResponse(200, username
						+ " does not exist in the records. Please register!");
			} else {
				decodedPassword = new String(
						Base64.decodeBase64(encodedPasword));
			}

			message = "DBPassword" + " " + decodedPassword+" "+emailId;
			response = new ClientResponse(201, message);

		} catch (SQLException e) {
			message = "Internal server error: " + e.getCause().getMessage();
			response = new ClientResponse(500, message);
		} catch (Exception e) {
			message = "Internal server error: " + e.getCause().getMessage();
			response = new ClientResponse(500, message);
		} finally {
			DBUtils.closeResources(rs, pst, connection);
		}
		return response;
	}

}
