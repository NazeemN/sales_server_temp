package in.fissionlabs.sales.utils;

public class URI_Constants {

	public static final String ADD_COMPANY = "/add/company";
	public static final String ADD_COMPANY_HOME = "/add";
	public static final String ADD_PROFILE = "/add/profile";
	public static final String ADD_COMPETITORS = "/add/competitors";
	public static final String ADD_TECHSTACK = "/add/techstack";
	public static final String ADD_FUNDINGS = "/add/funding";
	public static final String ADD_JOBS = "/add/jobs";
	public static final String ADD_LOCATIONS = "/add/locations";
	public static final String ADD_CONNECTIONS = "/add/connections";

	public static final String GET_COMPANY = "/{id}";
	public static final String GET_COMPANY_HOME = "/{id}/home";
	public static final String GET_PROFILE = "/{id}/profile";
	public static final String GET_COMPETITORS = "/{id}/competitors";
	public static final String GET_TECHSTACK = "/{id}/techstack";
	public static final String GET_FUNDINGS = "/{id}/funding";
	public static final String GET_JOBS = "/{id}/jobs";
	public static final String GET_LOCATIONS = "/{id}/locations";
	public static final String GET_CONNECTIONS = "/{id}/connections";
	
	public static final String LOGIN1 = "/login1";
	public static final String LOGIN = "/login";
	
	public static final String LOGOUT = "/logout";
	public static final String GET_COMPANYLIST = "/user/{username}";
	
	public static final String LOCATION_COMPANYLIST="/searchby/location/{searchString}";
	public static final String TECHNOLOGY_COMPANYLIST="/searchby/technology/{searchString}";

	public static final String CHECK_SESSION = "/checkSession";
	
	//public static final String ADD_STAGE = "/add/stage";
	//public static final String GET_STAGE = "/{id}/stage";
	public static final String ADD_STATUS = "/add/status";
	
	public static final String REFRESH = "/getData";
	public static final String SEND_MAIL = "/sendMail";
	public static final String ADD_REMARKS = "/addRemarks";
	
	public static final String DELETE_COMPANY  = "/{id}/delete";
	public static final String CONTROL_PANEL = "/getcp";
	public static final String UPDATE_USER = "/updateUsers";

	public static final String ADD_USER = "/addUser";
	public static final String RECOVER_PASSWORD ="/{username}/recover";
}
